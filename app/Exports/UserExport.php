<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserExport implements FromCollection, WithHeadings, WithMapping
{
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $queries = User::with('referal_user')->latest();
        if ($this->request->s) {
            $queries->where('mobile', 'like', '%' . $this->request->s . '%')->orWhere('name', 'like', '%' . $this->request->s . '%')->orWhere('email', 'like', '%' . $this->request->s . '%');
        }
        if ($this->request->user) {
            $queries->where('referal_id', $this->request->user);
        }
        if ($this->request->document_status) {
            $queries->where('is_document', $this->request->document_status);
        }
        if ($this->request->status) {
            $queries->where('status', $this->request->status);
        }
        $users = $queries->select()->paginate($this->request->limit);
        // dd($users);
        return $users;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Email',
            'Mobile',
            'Aadhar Number',
            'Pan Number',
            'Referal By',
            'Referal Code',
            'Lavel',
            'Bio',
            'Document Status',
            'Status',
            'Created At',
        ];
    }

    public function map($row): array
    {
        $fields = [
            $row->name,
            $row->email,
            $row->mobile,
            $row->aadhar_number,
            $row->pan_number,
            $row->referal_user ? $row->referal_user->name : '',
            $row->referal_code,
            'Lavel ' . $row->lavel,
            $row->bio,
            $row->is_document,
            $row->status,
            $row->created_at,
        ];
        return $fields;
    }
}
