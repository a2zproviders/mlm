<?php

namespace App\Exports;

use App\Models\Blog;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class BlogExport implements FromCollection, WithHeadings, WithMapping
{
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        // dd($this->request);
        // $blogs = Blog::all();

        $queries = Blog::latest();
        if ($this->request->s) {
            $queries->where('name', $this->request->s);
        }
        if ($this->request->category) {
            $queries->where('category_id', $this->request->category);
        }
        if ($this->request->status) {
            $queries->where('status', $this->request->status);
        }
        $blogs = $queries->select()->paginate($this->request->limit);
        return $blogs;
    }

    public function headings(): array
    {
        return [
            'Name',
            'slug',
            'Category',
            'Excerpt',
            'Descritpion',
            'Status',
            'Created At',
        ];
    }

    public function map($row): array
    {
        $fields = [
            $row->name,
            $row->slug,
            $row->category->name,
            $row->excerpt,
            $row->description,
            $row->status,
            $row->created_at,
        ];
        return $fields;
    }
}
