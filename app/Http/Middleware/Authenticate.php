<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            $guard = $request->route()->middleware()[1];
            $guard = explode(":", $guard)[1];

            if ($guard == 'admin') {
                return route('login');
            } else {
                return route('auth.login');
            }
        }
    }
}
