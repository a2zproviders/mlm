<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Kit;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\RegisterUser;
use App\Models\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class RegisterController extends BaseController
{
    public function index(Request $request, Kit $kit)
    {
        $page = 'register';
        $title = 'Register';
        $keywords = 'Register';
        $description = 'Register';

        $date =  date('Y-m-d');
        $date = strtotime($date . ' -18 year');
        $date =  date('Y-m-d', $date);

        $data = compact('page', 'title', 'keywords', 'description', 'kit', 'date');
        return view('frontend.guestlayout', $data);
    }

    public function store(Request $request, Kit $kit)
    {
        $rules = [
            "name"      => "required|string",
            "termcondition" => "required",
            "email"     => "required|email|unique:users",
            "mobile"     => "required|numeric|digits:10|unique:users"
        ];
        $request->validate($rules);

        $input = $request->except('_token');
        // $user = new User();
        // $user->fill($input);
        // $user->password = Hash::make($user->password);
        // $code = $this->generate_unique_username($request->name);
        // $user->referal_code = $code;
        if ($request->accept_code) {
            $referal_by = User::where('referal_code', $request->accept_code)->first();
            if ($referal_by) {
                $user_data = new RegisterUser();
                $user_data->data = json_encode($input);
                $user_data->save();

                return redirect(route('auth.kyc', [$user_data->id, $kit->slug]))->with('success', 'Please complete your KYC details. Do not skip step.');
            } else {
                return redirect()->back()->with('error', 'Accept code is not valid.');
            }
        }

        $user_data = new RegisterUser();
        $user_data->data = json_encode($input);
        $user_data->save();

        return redirect(route('auth.kyc', [$user_data->id, $kit->slug]))->with('success', 'Please complete your KYC details. Do not skip step.');
    }

    public function kyc(Request $request, $id, Kit $kit)
    {
        $page = 'kyc';
        $title = 'Register';
        $keywords = 'Register';
        $description = 'Register';

        $data = compact('page', 'title', 'keywords', 'description', 'kit', 'id');
        return view('frontend.guestlayout', $data);
    }

    protected function username_exist_in_database($username)
    {
        $user = User::where('referal_code', $username)->count();
        return $user;
    }

    protected function generate_unique_username($string_name, $rand_no = 99999)
    {
        while (true) {
            $username_parts = array_filter(explode(" ", strtolower($string_name))); //explode and lowercase name
            $username_parts = array_slice($username_parts, 0, 2); //return only first two arry part

            $part1 = (!empty($username_parts[0])) ? substr($username_parts[0], 0, 8) : ""; //cut first name to 8 letters
            $part2 = (!empty($username_parts[1])) ? substr($username_parts[1], 0, 5) : ""; //cut second name to 5 letters
            $part3 = ($rand_no) ? rand(0, $rand_no) : "";

            $username = $part1 . str_shuffle($part2) . $part3; //str_shuffle to randomly shuffle all characters 
            if (strlen($username) > 6) {
                $username_exist_in_db = $this->username_exist_in_database($username); //check username in database
                if (!$username_exist_in_db) {
                    return $username;
                }
            }
        }
    }
    public function register(Request $request, $id, Kit $kit)
    {
        $rules = [
            "txn_id" => "required",
            "aadhar_number" => "required",
            "pan_number"     => "required",
        ];
        $request->validate($rules);
        $user_data = RegisterUser::find($id);
        $details = json_decode($user_data->data);
        $input = $request->except('_token', 'aadhar_front', 'aadhar_back', 'pan_image', 'image', 'txn_id');
        $user = new User();
        $user->fill($input);
        $user->name     = $details->name;
        $user->email    = $details->email;
        $user->mobile   = $details->mobile;
        $user->dateofbirth = $details->dateofbirth;
        $user->password = Hash::make($details->password);
        $user->email    = $details->email;
        $user->email    = $details->email;
        $code = $this->generate_unique_username($details->name);
        $user->referal_code = $code;
        if ($details->accept_code) {
            $referal_by = User::where('referal_code', $details->accept_code)->first();
            if ($referal_by) {
                $lavel = $referal_by->lavel + 1;
                $user->lavel = $lavel;
                $user->referal_id = $referal_by->id;
            }
        }
        if ($details->termcondition) {
            $user->termcondition = true;
        }
        // dd($kit);
        // dd($request);
        $user->save();

        $address = new Address();
        $address->user_id   = $user->id;
        $address->name      = $user->name;
        $address->email     = $user->email;
        $address->mobile    = $user->mobile;
        $address->state     = $details->state;
        $address->city      = $details->city;
        $address->pincode   = $details->pincode;
        $address->address   = $details->address;
        $address->save();

        if ($request->hasFile('aadhar_front')) {
            $file  = $request->file('aadhar_front');
            $name  = 'IMG_front' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/aadharcard/' . $name, (string) $image->encode());
            $user->update(['aadhar_front' => $name]);
        }
        if ($request->hasFile('aadhar_back')) {
            $file  = $request->file('aadhar_back');
            $name  = 'IMG_back' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/aadharcard/' . $name, (string) $image->encode());
            $user->update(['aadhar_back' => $name]);
        }
        if ($request->hasFile('pan_image')) {
            $file  = $request->file('pan_image');
            $name  = 'IMG' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/pancard/' . $name, (string) $image->encode());
            $user->update(['pan_image' => $name]);
        }
        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            $name  = 'IMG' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/user/' . $name, (string) $image->encode());
            $user->update(['image' => $name]);
        }

        // order

        $currentMonth   = date('n', time());
        $financialYear  = $currentMonth > 3 ? date('Y') . "-" . (date('y') + 1) : (date('Y') - 1) . "-" . date('y');

        $maxInvoice     = Order::where('financial_year', $financialYear)->max('invoice_no');
        $invocieNo      = $maxInvoice + 1;

        $input1 = [
            'user_id' => $user->id,
            'address_id' => $address->id,
            'kit_id' => $kit->id,
            'financial_year' => $financialYear,
            'invoice_no' => $invocieNo,
            'txn_id' => $request->txn_id,
            'amount' => $kit->price,
            'delivary_charge' => 0,
            'total_amount' => $kit->price,
            'payment_type' => 'online',
            'payment_status' => 'paid',
        ];
        $order = new Order();
        $order->fill($input1);
        $order->save();

        foreach ($kit->products as $key => $pro) {
            $input2 = [
                'order_id' => $order->id,
                'product_id' => $pro->product->id,
                'name' => $pro->product->name,
                'price' => $pro->product->price,
                'discount' => $pro->product->discount,
                'sale_price' => $pro->product->sale_price,
            ];
            $order_product = new OrderProduct();
            $order_product->fill($input2);
            $order_product->save();
        }

        $register_data = RegisterUser::find($id);
        $register_data->delete();

        return redirect(route('auth.login'))->with('success', 'User registered successfully. Please login.');
    }




    public function forgot()
    {
        $page = 'forgot';
        $title = 'Forgot Password';
        $keywords = 'Forgot Password';
        $description = 'Forgot Password';

        $data = compact('page', 'title', 'keywords', 'description');
        return view('frontend.guestlayout', $data);
    }
}
