<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AjexController extends BaseController
{
    public function check(Request $request)
    {
        $status = true;
        $errors = [];
        $check_email = User::where('email', $request->email)->count();
        if ($check_email) {
            $errors[] = 'This email already exists.';
            $status = false;
        }
        $check_mobile = User::where('mobile', $request->mobile)->count();
        if ($check_mobile) {
            $errors[] = 'This mobile number already exists.';
            $status = false;
        }
        if ($request->accept_code) {
            $check_code = User::where('referal_code', $request->accept_code)->count();
            if (!$check_code) {
                $errors[] = 'This code is not valid.';
                $status = false;
            }
        }
        if (!$status) {
            $data = view('frontend.templete.message', compact('errors'))->render();
            $re = [
                'status' => $status,
                'data' => $data
            ];
        } else {
            $second_step = view('frontend.templete.second_step')->render();
            $re = [
                'status' => $status,
                'message' => 'Please fill complete details.',
                'second_step' => $second_step
            ];
        }
        return response($re);
    }

    public function checklogin(Request $request)
    {
        $rules = [
            "email"       => "required|email",
            "password"    => "required",
        ];
        $request->validate($rules);

        $crediencial = array(
            'email'     => $request->email,
            'password'  => $request->password
        );
        $is_remembered = !empty($request->remember_me) ? true : false;
        if (Auth::guard('admin')->attempt($crediencial, $is_remembered)) {
            return redirect(route('admin.dashboard'))->with('success', 'You are successfully login.');
        } else {
            return redirect()->back()->with('error', 'Credentials not matched.');
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        return redirect(route('login'))->with('success', 'You are successfully logout.');
    }
}
