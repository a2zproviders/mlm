<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Faq;
use App\Models\Kit;
use App\Models\Page;
use App\Models\Product;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends BaseController
{
    public function index()
    {
        $kits = Kit::where('status', 'true')->paginate(4);
        $blogs = Blog::where('status', 'true')->paginate(4);
        $about_us = Page::where('slug', 'about-us')->first();
        $faqs = Faq::get();

        $data = compact('kits', 'faqs', 'blogs', 'about_us');
        return view('frontend.inc.homepage', $data);
    }

    public function kit(Request $request)
    {
        $query = Kit::where('status', true);
        if ($request->s) {
            $query->where('name', 'LIKE', '%' . $request->s . '%');
        }
        $lists = $query->get();
        $popular_kits = Kit::where('status', true)->get();

        $data = compact('lists', 'popular_kits');
        return view('frontend.inc.kit', $data);
    }

    public function kitdetail(Kit $kit)
    {
        $popular_kits = Kit::where('status', true)->get();

        $data = compact('kit', 'popular_kits');
        return view('frontend.inc.kitdetail', $data);
    }

    // public function product()
    // {
    //     return view('frontend.inc.product');
    // }

    public function productdetail(Product $product)
    {
        $popular_kits = Kit::where('status', true)->get();

        $data = compact('product', 'popular_kits');
        return view('frontend.inc.productdetail', $data);
    }

    public function blog(Request $request)
    {
        $query = Blog::where('status', true);
        if ($request->s) {
            $query->where('name', 'LIKE', '%' . $request->s . '%');
        }
        $blogs = $query->get();
        $popular_kits = Kit::where('status', true)->paginate(10);
        $popular_blogs = Blog::where('status', true)->paginate(10);

        $data = compact('blogs', 'popular_kits', 'popular_blogs');
        return view('frontend.inc.blog', $data);
    }

    public function blogdetail(Blog $blog)
    {
        $popular_blogs = Blog::where('status', true)->paginate(10);

        $data = compact('blog', 'popular_blogs');
        return view('frontend.inc.blogdetail', $data);
    }

    public function termcondition()
    {
        $page = Page::where('slug', 'about-us')->first();

        $data = compact('page');
        return view('frontend.inc.termcondition', $data);
    }
}
