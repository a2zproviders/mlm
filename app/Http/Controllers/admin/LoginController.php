<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends BaseController
{
    public function index()
    {
        $page = 'login';
        $title = 'Admin Login';

        $data = compact('page', 'title');
        return view('backend.guestlayout', $data);
    }

    public function checklogin(Request $request)
    {
        $rules = [
            "email"       => "required|email",
            "password"    => "required",
        ];
        $request->validate($rules);

        $crediencial = array(
            'email'     => $request->email,
            'password'  => $request->password
        );
        $is_remembered = !empty($request->remember_me) ? true : false;
        if (Auth::guard('admin')->attempt($crediencial, $is_remembered)) {
            return redirect(route('admin.dashboard'))->with('success', 'You are successfully login.');
        } else {
            return redirect()->back()->with('error', 'Credentials not matched.');
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        return redirect(route('login'))->with('success', 'You are successfully logout.');
    }
}
