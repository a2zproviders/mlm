<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Kit;
use App\Models\KitProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'faqs.list';
        $title = 'Faqs List';

        $lists = Faq::paginate(10);

        $data = compact('page', 'title', 'lists');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'faqs.add';
        $title = 'Add Faqs';

        $data = compact('page', 'title');
        return view('backend.layout', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name"      => "required",
            "description"     => "required",
        ];
        $request->validate($rules);

        $input = $request->except('_token');
        $faq = new Faq();
        // dd($request);
        $faq->fill($input);
        $faq->save();

        return redirect(route('admin.faqs.index'))->with('success', 'New record added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Faq $faq)
    {
        $request->replace($faq->toArray());
        $request->flash();

        $page = 'faqs.edit';
        $title = 'Edit Faqs';

        $data = compact('page', 'title', 'faq');
        return view('backend.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        $rules = [
            "name"      => "required",
            "description"     => "required",
        ];
        $request->validate($rules);

        $input = $request->except('_token');
        $faq->update($input);

        return redirect(route('admin.faqs.index'))->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Faq $faq)
    {
        //
    }

    public function delete(Faq $faq)
    {
        $faq->delete();

        return redirect(route('admin.faqs.index'))->with('success', 'Record deleted successfully.');
    }
}
