<?php

namespace App\Http\Controllers\admin;

use App\Exports\UserExport;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Excel;
use PDF;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Excel as ExcelExcel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = 'user.list';
        $title = 'Users List';

        $queries = User::with('members')->latest();
        if ($request->s) {
            $queries->where('mobile', 'like', '%' . $request->s . '%')->orWhere('name', 'like', '%' . $request->s . '%')->orWhere('email', 'like', '%' . $request->s . '%');
        }
        if ($request->user) {
            $queries->where('referal_id', $request->user);
        }
        if ($request->document_status) {
            $queries->where('is_document', $request->document_status);
        }
        if ($request->status) {
            $queries->where('status', $request->status);
        }
        $lists = $queries->paginate($limit);
        $query = $request->all();
        $users = User::get();

        $data = compact('page', 'title', 'lists', 'users', 'query');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'user.add';
        $title = 'Add User';

        $users = User::orderBy('name', 'desc')->get();
        $userArr  = [];
        if (!$users->isEmpty()) {
            foreach ($users as $user) {
                $userArr[$user->id] = $user->name;
            }
        }

        $data = compact('page', 'title', 'userArr');
        return view('backend.layout', $data);
    }

    protected function username_exist_in_database($username)
    {
        $user = User::where('referal_code', $username)->count();
        return $user;
    }

    protected function generate_unique_username($string_name, $rand_no = 99999)
    {
        while (true) {
            $username_parts = array_filter(explode(" ", strtolower($string_name))); //explode and lowercase name
            $username_parts = array_slice($username_parts, 0, 2); //return only first two arry part

            $part1 = (!empty($username_parts[0])) ? substr($username_parts[0], 0, 8) : ""; //cut first name to 8 letters
            $part2 = (!empty($username_parts[1])) ? substr($username_parts[1], 0, 5) : ""; //cut second name to 5 letters
            $part3 = ($rand_no) ? rand(0, $rand_no) : "";

            $username = $part1 . str_shuffle($part2) . $part3; //str_shuffle to randomly shuffle all characters 
            if (strlen($username) > 6) {
                $username_exist_in_db = $this->username_exist_in_database($username); //check username in database
                if (!$username_exist_in_db) {
                    return $username;
                }
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name"      => "required",
            "email"     => "required|email|unique:users",
            "mobile"     => "required|numeric|digits:10|unique:users",
            "aadhar_number" => "required|numeric|digits:12",
            "pan_number"     => "required|size:10",
        ];
        $request->validate($rules);

        $input = $request->except('_token', 'aadhar_front', 'aadhar_back', 'pan_image');
        $user = new User;
        $user->fill($input);
        $user->password = Hash::make($user->password);
        $code = $this->generate_unique_username($request->name);
        $user->referal_code = $code;
        if ($request->accept_code) {
            $referal_by = User::where('referal_code', $request->accept_code)->first();
            if ($referal_by) {
                $lavel = $referal_by->lavel + 1;
                $user->lavel = $lavel;
                $user->referal_id = $referal_by->id;
            } else {
                return redirect()->back()->with('error', 'Accept code is not valid.');
            }
        }
        $user->save();

        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            $name  = 'IMG_' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/user/' . $name, (string) $image->encode());
            $user->update(['image' => $name]);
        }
        if ($request->hasFile('aadhar_front')) {
            $file  = $request->file('aadhar_front');
            $name  = 'IMG_front' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/aadharcard/' . $name, (string) $image->encode());
            $user->update(['aadhar_front' => $name]);
        }
        if ($request->hasFile('aadhar_back')) {
            $file  = $request->file('aadhar_back');
            $name  = 'IMG_back' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/aadharcard/' . $name, (string) $image->encode());
            $user->update(['aadhar_back' => $name]);
        }
        if ($request->hasFile('pan_image')) {
            $file  = $request->file('pan_image');
            $name  = 'IMG' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/pancard/' . $name, (string) $image->encode());
            $user->update(['pan_image' => $name]);
        }

        return redirect(route('admin.user.index'))->with('success', 'New record added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        $page = 'user.profile';
        $title = 'User Profile';
        $user = User::with('members')->find($user->id);

        $data = compact('page', 'title', 'user');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $user)
    {
        $request->replace($user->toArray());
        $request->flash();

        $page = 'user.edit';
        $title = 'Edit User';

        $users = User::orderBy('name', 'desc')->whereNotIn('id', [$user->id])->get();
        $userArr  = [];
        if (!$users->isEmpty()) {
            foreach ($users as $u) {
                $userArr[$u->id] = $u->name;
            }
        }

        $data = compact('page', 'title', 'user', 'userArr');
        return view('backend.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            "name"      => "required",
            "email"     => "required|email|unique:users,email," . $user->id,
            "mobile"     => "required|numeric|digits:10|unique:users,mobile," . $user->id,
            "aadhar_number" => "required|numeric|digits:12",
            "pan_number"     => "required|size:10",
        ];
        $request->validate($rules);

        $input = $request->except('_method', '_token', 'aadhar_front', 'aadhar_back', 'pan_image', 'password');
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }
        // $user->fill($input);
        $user->update($input);


        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            $name  = 'IMG_' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/user/' . $name, (string) $image->encode());
            $user->update(['image' => $name]);
        }
        if ($request->hasFile('aadhar_front')) {
            $file  = $request->file('aadhar_front');
            $name  = 'IMG_front' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/aadharcard/' . $name, (string) $image->encode());
            $user->update(['aadhar_front' => $name]);
        }
        if ($request->hasFile('aadhar_back')) {
            $file  = $request->file('aadhar_back');
            $name  = 'IMG_back' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/aadharcard/' . $name, (string) $image->encode());
            $user->update(['aadhar_back' => $name]);
        }
        if ($request->hasFile('pan_image')) {
            $file  = $request->file('pan_image');
            $name  = 'IMG' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/pancard/' . $name, (string) $image->encode());
            $user->update(['pan_image' => $name]);
        }

        return redirect(route('admin.user.index'))->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect(route('admin.user.index'))->with('success', 'Record deleted successfully.');
    }

    public function delete(User $user)
    {
        $user->delete();

        return redirect(route('admin.user.index'))->with('success', 'Record deleted successfully.');
    }
    public function documentstatus(User $user)
    {
        if ($user->is_document == 'pending') {
            $user->is_document = 'completed';
        } else {
            $user->is_document = 'pending';
        }
        $user->save();

        return redirect(route('admin.user.index'))->with('success', 'Record document verify status changed successfully.');
    }

    public function status(User $user)
    {
        if ($user->status == 'true') {
            $user->status = 'false';
        } else {
            $user->status = 'true';
        }
        $user->save();

        return redirect(route('admin.user.index'))->with('success', 'Record status changed successfully.');
    }

    public function csv(Request $request)
    {
        return \Excel::download(new UserExport($request), 'users.csv');
    }

    public function pdf(Request $request)
    {
        $queries = User::latest();
        if ($request->s) {
            $queries->where('mobile', 'like', '%' . $request->s . '%')->orWhere('name', 'like', '%' . $request->s . '%')->orWhere('email', 'like', '%' . $request->s . '%');
        }
        if ($request->user) {
            $queries->where('referal_id', $request->user);
        }
        if ($request->document_status) {
            $queries->where('is_document', $request->document_status);
        }
        if ($request->status) {
            $queries->where('status', $request->status);
        }
        $data = $queries->paginate($request->limit);

        view()->share('lists', $data);
        $pdf = PDF::loadView('backend.pdf.user', $data);

        // return $pdf->download('pdf_file.pdf');
        return $pdf->stream('result.pdf');
    }
}
