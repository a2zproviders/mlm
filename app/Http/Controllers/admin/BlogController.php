<?php

namespace App\Http\Controllers\admin;

use App\Exports\BlogExport;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
use Illuminate\Http\Request;
use Excel;
use PDF;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = 'blog.list';
        $title = 'Blog';

        $queries = Blog::latest();
        if ($request->s) {
            $queries->where('name', $request->s);
        }
        if ($request->category) {
            $queries->where('category_id', $request->category);
        }
        if ($request->status) {
            $queries->where('status', $request->status);
        }
        $lists = $queries->paginate($limit);

        $query = $request->all();
        $categories = BlogCategory::get();

        $data = compact('page', 'title', 'lists', 'categories', 'query');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'blog.add';
        $title = 'Blog Add';

        $categories = BlogCategory::orderBy('name', 'desc')->get();
        $categoryArr  = ['' => 'Select Category'];
        if (!$categories->isEmpty()) {
            foreach ($categories as $category) {
                $categoryArr[$category->id] = $category->name;
            }
        }

        $data = compact('page', 'title', 'categoryArr');
        return view('backend.layout', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name"      => "required",
            "slug"      => "unique:blogs",
            "category_id"  => "required",
        ];
        $request->validate($rules);

        $input = $request->except('_token', 'image');
        $blog = new Blog();
        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $blog->fill($input);
        $blog->save();

        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            // $uniq_id = uniqid();
            $name  = 'IMG_'  . $blog->id . '.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/blog/' . $name, (string) $image->encode());
            // $blog->image = $name;
            $blog->update(['image' => $name]);
        }

        return redirect(route('admin.blog.index'))->with('success', 'New record added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        $page = 'blog.view';
        $title = 'View Product';

        $data = compact('page', 'title', 'blog');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Blog $blog)
    {
        $request->replace($blog->toArray());
        $request->flash();

        $page = 'blog.edit';
        $title = 'Edit Product';

        $categories = BlogCategory::orderBy('name', 'desc')->get();
        $categoryArr  = ['' => 'Select Category'];
        if (!$categories->isEmpty()) {
            foreach ($categories as $category) {
                $categoryArr[$category->id] = $category->name;
            }
        }

        $data = compact('page', 'title', 'blog', 'categoryArr');
        return view('backend.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $rules = [
            "name"      => "required",
            "slug"      => "required|unique:blogs,slug," . $blog->id,
            "category_id"  => "required",
        ];
        $request->validate($rules);

        $input = $request->except('_method', '_token', 'image');
        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $blog->update($input);

        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            // $uniq_id = uniqid();
            $name  = 'IMG_'  . $blog->id . '.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/blog/' . $name, (string) $image->encode());
            // $blog->image = $name;
            $blog->update(['image' => $name]);
        }

        return redirect(route('admin.blog.index'))->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $blog->delete();

        return redirect(route('admin.blog.index'))->with('success', 'Record deleted successfully.');
    }

    public function delete(Blog $blog)
    {
        $blog->delete();

        return redirect(route('admin.blog.index'))->with('success', 'Record deleted successfully.');
    }

    public function status(Blog $blog)
    {
        if ($blog->status == 'true') {
            $blog->status = 'false';
        } else {
            $blog->status = 'true';
        }
        $blog->save();

        return redirect(route('admin.blog.index'))->with('success', 'Record status changed successfully.');
    }

    public function csv(Request $request)
    {
        return Excel::download(new BlogExport($request), 'blogs.csv');
        // return (new BlogExport)->download('invoices.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }

    public function pdf(Request $request)
    {
        $queries = Blog::latest();
        if ($request->s) {
            $queries->where('name', $request->s);
        }
        if ($request->category) {
            $queries->where('category_id', $request->category);
        }
        if ($request->status) {
            $queries->where('status', $request->status);
        }
        $data = $queries->paginate($request->limit);

        view()->share('blogs', $data);
        $pdf = PDF::loadView('backend.pdf.blog', $data);

        // return $pdf->download('pdf_file.pdf');
        return $pdf->stream('result.pdf');
    }
}
