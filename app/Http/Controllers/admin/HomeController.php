<?php

namespace App\Http\Controllers\admin;

use App\Models\Order;
use App\Models\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class HomeController extends BaseController
{
    public function index()
    {
        $page = 'dashboard';
        $title = 'Dashboard';
        $user = Auth::guard('admin')->user();

        $chart_user = User::select(\DB::raw('count(id) as `data`'), \DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
            ->groupby('year', 'month')
            ->get();
        $chart_users = [];
        foreach ($chart_user as $key => $u) {
            $chart_users[] = $u->data;
        }
        $chart_users = implode(',', $chart_users);
        $chart_income = [];
        $chart_income = implode(',', $chart_income);
        $members = User::get();
        $orders = Order::get();
        $data = compact('page', 'title','members', 'orders', 'chart_users', 'user', 'chart_income');
                
        return view('backend.layout', $data);
    }

    public function form()
    {
        $page = 'setting';
        $title = 'Setting';

        $data = compact('page', 'title');
        return view('backend.layout', $data);
    }
}
