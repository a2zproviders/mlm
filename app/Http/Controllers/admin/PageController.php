<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\Page;
use Illuminate\Http\Request;
use Excel;
use PDF;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = 'page.list';
        $title = 'Page';

        $queries = Page::latest();
        if ($request->s) {
            $queries->where('name', $request->s);
        }
        $lists = $queries->paginate($limit);

        $data = compact('page', 'title', 'lists');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'page.add';
        $title = 'Page Add';

        $data = compact('page', 'title');
        return view('backend.layout', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name"      => "required",
            "slug"      => "unique:pages",
        ];
        $request->validate($rules);

        $input = $request->except('_token', 'image');
        $page = new Page();
        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $page->fill($input);
        $page->save();

        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            // $uniq_id = uniqid();
            $name  = 'IMG_'  . $page->id . '.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/page/' . $name, (string) $image->encode());
            // $page->image = $name;
            $page->update(['image' => $name]);
        }

        return redirect(route('admin.page.index'))->with('success', 'New record added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        $page = 'page.view';
        $title = 'View Page';

        $data = compact('page', 'title', 'page');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Page $page)
    {
        $request->replace($page->toArray());
        $request->flash();
        $page_data = $page;

        $page = 'page.edit';
        $title = 'Edit Page';

        $data = compact('page', 'title', 'page_data');
        return view('backend.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $rules = [
            "name"      => "required",
            "slug"      => "required|unique:pages,slug," . $page->id,
        ];
        $request->validate($rules);

        $input = $request->except('_method', '_token', 'image');
        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $page->update($input);

        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            // $uniq_id = uniqid();
            $name  = 'IMG_'  . $page->id . '.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/page/' . $name, (string) $image->encode());
            // $page->image = $name;
            $page->update(['image' => $name]);
        }

        return redirect(route('admin.page.index'))->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();

        return redirect(route('admin.page.index'))->with('success', 'Record deleted successfully.');
    }

    public function delete(Page $page)
    {
        $page->delete();

        return redirect(route('admin.page.index'))->with('success', 'Record deleted successfully.');
    }

}
