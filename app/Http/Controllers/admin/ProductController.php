<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 'product.list';
        $title = 'Product';

        $lists = Product::withCount('images')->get();

        $data = compact('page', 'title', 'lists');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'product.add';
        $title = 'Product Add';

        $data = compact('page', 'title');
        return view('backend.layout', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name"      => "required",
            "slug"      => "unique:products",
            "price"     => "required|numeric",
        ];
        $request->validate($rules);

        $input = $request->except('_token', 'image');
        $product = new Product;
        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $product->fill($input);
        $product->save();

        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            $uniq_id = uniqid();
            $name  = 'IMG_' . '_' . $product->id . '_' . $uniq_id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/product/' . $name, (string) $image->encode());
            $product_image = new ProductImage();
            $product_image->product_id = $product->id;
            $product_image->image = $name;
            $product_image->save();
        }

        return redirect(route('admin.product.index'))->with('success', 'New record added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $page = 'product.view';
        $title = 'View Product';

        $data = compact('page', 'title', 'product');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Product $product)
    {
        $request->replace($product->toArray());
        $request->flash();

        $page = 'product.edit';
        $title = 'Edit Product';

        $data = compact('page', 'title', 'product');
        return view('backend.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $rules = [
            "name"      => "required",
            "slug"      => "required|unique:products,slug," . $product->id,
            "price"     => "required|numeric",
        ];
        $request->validate($rules);

        $input = $request->except('_method', '_token', 'image');
        $product->fill($input);
        $product->save();

        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            $uniq_id = uniqid();
            $name  = 'IMG_' . '_' . $product->id . '_' . $uniq_id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/product/' . $name, (string) $image->encode());
            $product_image = new ProductImage();
            $product_image->product_id = $product->id;
            $product_image->image = $name;
            $product_image->save();
        }

        return redirect(route('admin.product.index'))->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect(route('admin.product.index'))->with('success', 'Record deleted successfully.');
    }

    public function delete(Product $product)
    {
        $product->images()->delete();
        $product->delete();

        return redirect(route('admin.product.index'))->with('success', 'Record deleted successfully.');
    }
    public function status(Product $product)
    {
        if ($product->status == 'true') {
            $product->status = 'false';
        } else{
                $product->status = 'true';
        }
        $product->save();

        return redirect(route('admin.product.index'))->with('success', 'Record status changed successfully.');
    }
}
