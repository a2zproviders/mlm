<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Kit;
use App\Models\KitProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class KitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'kit.list';
        $title = 'Kit List';

        $lists = Kit::paginate(10);

        $data = compact('page', 'title', 'lists');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'kit.add';
        $title = 'Add Kit';

        $products = Product::orderBy('name', 'desc')->where('status', 'true')->get();
        $productArr  = [];
        if (!$products->isEmpty()) {
            foreach ($products as $product) {
                $productArr[$product->id] = $product->name;
            }
        }

        $data = compact('page', 'title', 'productArr');
        return view('backend.layout', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name"      => "required",
            "slug"      => "unique:kits",
            "price"     => "required|numeric",
        ];
        $request->validate($rules);

        $input = $request->except('_token', 'image', 'products');
        $kit = new Kit;
        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        // dd($request);
        $kit->fill($input);
        $kit->save();

        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            // $uniq_id = uniqid();
            $name  = 'IMG_' . $kit->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/kit/' . $name, (string) $image->encode());
            $kit->image = $name;
            $kit->save();
        }
        if ($request->products) {
            foreach ($request->products as $key => $value) {
                $productArr = ['product_id' => $value];
                $kit->products()->create($productArr);
            }
        }

        return redirect(route('admin.kit.index'))->with('success', 'New record added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kit $kit)
    {
        $page = 'kit.view';
        $title = 'View Kit';

        $data = compact('page', 'title', 'kit');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Kit $kit)
    {
        $request->replace($kit->toArray());
        $request->flash();

        $page = 'kit.edit';
        $title = 'Edit Kit';
        $selectedArr = [];
        if ($kit->products) {
            foreach ($kit->products as $product) {
                $selectedArr[] = $product->product_id;
            }
        }

        $products = Product::orderBy('name', 'desc')->where('status', 'true')->get();
        $productArr  = [];
        if (!$products->isEmpty()) {
            foreach ($products as $product) {
                $productArr[$product->id] = $product->name;
            }
        }

        $data = compact('page', 'title', 'kit', 'productArr', 'selectedArr');
        return view('backend.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kit $kit)
    {
        $rules = [
            "name"      => "required",
            "slug"      => "unique:kits,slug," . $kit->id,
            "price"     => "required|numeric",
        ];
        $request->validate($rules);

        $input = $request->except('_token', 'image', 'products');

        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $kit->update($input);

        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            // $uniq_id = uniqid();
            $name  = 'IMG_' . $kit->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/kit/' . $name, (string) $image->encode());
            $kit->update(['image' => $name]);
        }
        if ($request->products) {
            KitProduct::where('kit_id', $kit->id)->delete();
            foreach ($request->products as $key => $value) {
                // $kitproduct = KitProduct::where('kit_id', $kit->id)->where('product_id', $value)->exists();
                $productArr = ['product_id' => $value];
                $kit->products()->create($productArr);
            }
        }

        return redirect(route('admin.kit.index'))->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Kit $kit)
    {
        //
    }

    public function delete(Kit $kit)
    {
        KitProduct::where('kit_id', $kit->id)->delete();
        $kit->delete();

        return redirect(route('admin.kit.index'))->with('success', 'Record deleted successfully.');
    }
    public function get_products_price(Request $request)
    {
        $price = 0;
        if ($request->products) {
            foreach ($request->products as $key => $value) {
                $product = Product::select('price')->find($value);
                $price += $product->price;
            }
        }
        $re = [
            'price' => $price,
        ];
        return response()->json($re);
    }

    public function status(Kit $kit)
    {
        if ($kit->status == 'true') {
            $kit->status = 'false';
        } else {
            $kit->status = 'true';
        }
        $kit->save();

        return redirect(route('admin.kit.index'))->with('success', 'Record status changed successfully.');
    }
}
