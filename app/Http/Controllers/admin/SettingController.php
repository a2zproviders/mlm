<?php

namespace App\Http\Controllers\admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class SettingController extends BaseController
{
    public function index(Request $request)
    {
        $page = 'setting';
        $title = 'Setting';
        $setting = Setting::findOrFail(1);
        $request->replace($setting->toArray());
        $request->flash();

        $data = compact('page', 'title', 'setting');
        return view('backend.layout', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            "site_name" => "required",
            "email"     => "required",
        ];
        $request->validate($rules);

        $input = $request->except('_token');
        $setting = Setting::findOrFail(1);
        $setting->fill($input);
        $setting->save();

        return redirect()->back()->with('success', 'Settings updated successfully.');
    }
}
