<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\BlogCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'blogcategory.list';
        $title = 'Blog Category List';

        $lists = BlogCategory::paginate(10);

        $data = compact('page', 'title', 'lists');
        return view('backend.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'blogcategory.add';
        $title = 'Add Blog Category';

        $data = compact('page', 'title');
        return view('backend.layout', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name"      => "required",
            "slug"      => "unique:blog_categories",
        ];
        $request->validate($rules);

        $input = $request->except('_token');
        $blogcategory = new BlogCategory();
        // dd($request);
        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $blogcategory->fill($input);
        $blogcategory->save();

        return redirect(route('admin.blogcategory.index'))->with('success', 'New record added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, BlogCategory $blogcategory)
    {
        $request->replace($blogcategory->toArray());
        $request->flash();

        $page = 'blogcategory.edit';
        $title = 'Edit Blog Category';

        $data = compact('page', 'title', 'blogcategory');
        return view('backend.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogCategory $blogcategory)
    {
        $rules = [
            "name"      => "required",
            "description"     => "required",
        ];
        $request->validate($rules);

        $input = $request->except('_token');
        $blogcategory->update($input);

        return redirect(route('admin.blogcategory.index'))->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, BlogCategory $blogcategory)
    {
        //
    }

    public function delete(BlogCategory $blogcategory)
    {
        $blogcategory->delete();

        return redirect(route('admin.blogcategory.index'))->with('success', 'Record deleted successfully.');
    }
}
