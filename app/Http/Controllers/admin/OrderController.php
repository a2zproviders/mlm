<?php

namespace App\Http\Controllers\admin;

use App\Models\Address;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\Setting;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends BaseController
{
    public function index(Request $request)
    {
        $limit  = $request->limit ? $request->limit : 10;
        $lists = Order::paginate($limit)->appends(request()->query());

        $page = 'order.list';
        $title = 'Order';
        $data = compact('page', 'title', 'lists');
        return view('backend.layout', $data);
    }
    public function status(Request $request, Order $order)
    {
        if ($request->status) {
            $order->status = $request->status;
        }
        $order->save();
        $req_all = $request->all();
        array_pop($req_all);

        return redirect(route('admin.order', $req_all))->with('success', 'Order status changed successfully.');
    }

    public function delete(Request $request, Order $order)
    {
        $orderproduct = OrderProduct::where('order_id', $order->id)->delete();
        $order->delete();
        return redirect()->back()->with('success', 'Order has been deleted successfully.');
    }

    public function show(Request $request, Order $order)
    {
        $page = 'order.invoice';
        $title = 'Order';
        $setting = Setting::find(1);
        $data = compact('page', 'title', 'order', 'setting');
        return view('backend.layout', $data);
    }
}
