<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\Setting;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends BaseController
{
    public function index(Request $request)
    {
        $limit  = $request->limit ? $request->limit : 10;
        $user = Auth::guard('user')->user();
        $lists = Order::where('user_id', $user->id)->paginate($limit)->appends(request()->query());

        $data = compact('lists');
        return view('frontend.inc.user.order', $data);
    }
    public function status(Request $request, Order $order)
    {
        if ($request->status) {
            $order->status = $request->status;
        }
        $order->save();
        $req_all = $request->all();
        array_pop($req_all);

        return redirect(route('user.order', $req_all))->with('success', 'Order status changed successfully.');
    }

    public function show(Request $request, Order $order)
    {

        $setting = Setting::find(1);

        $data = compact('order', 'setting');
        return view('frontend.inc.user.invoice', $data);
    }
}
