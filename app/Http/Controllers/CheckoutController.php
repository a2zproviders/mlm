<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Kit;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends BaseController
{
    public function index(Kit $kit)
    {
        $user = Auth::guard('user')->user();
        $addresses = Address::where('user_id', $user->id)->get();

        $data = compact('kit', 'user', 'addresses');
        return view('frontend.inc.user.checkout', $data);
    }
    public function store(Request $request)
    {
        $kit = Kit::find($request->product_id);
        $user = Auth::guard('user')->user();
        // dd($kit);

        $currentMonth   = date('n', time());
        $financialYear  = $currentMonth > 3 ? date('Y') . "-" . (date('y') + 1) : (date('Y') - 1) . "-" . date('y');

        $maxInvoice     = Order::where('financial_year', $financialYear)->max('invoice_no');
        $invocieNo      = $maxInvoice + 1;

        $input = [
            'user_id' => $user->id,
            'address_id' => $request->address_id,
            'kit_id' => $request->product_id,
            'financial_year' => $financialYear,
            'invoice_no' => $invocieNo,
            'txn_id' => $request->txn_id,
            'amount' => $request->subtotal,
            'delivary_charge' => $request->shipping,
            'total_amount' => $request->total,
            'payment_type' => 'online',
            'payment_status' => 'paid',
        ];
        $order = new Order();
        $order->fill($input);
        $order->save();
        
        foreach ($kit->products as $key => $pro) {
            $input2 = [
                'order_id' => $order->id,
                'product_id' => $pro->product->id,
                'name' => $pro->product->name,
                'price' => $pro->product->price,
                'discount' => $pro->product->discount,
                'sale_price' => $pro->product->sale_price,
            ];
            $order_product = new OrderProduct();
            $order_product->fill($input2);
            $order_product->save();
        }

        return response()->json($order);
    }
}
