<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UserController extends BaseController
{
    public function index()
    {
        $user = Auth::guard('user')->user();
        $chart_user = User::where('referal_id', $user->id)->select(\DB::raw('count(id) as `data`'), \DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
            ->groupby('year', 'month')
            ->get();
        $chart_users = [];
        foreach ($chart_user as $key => $u) {
            $chart_users[] = $u->data;
        }
        $chart_users = implode(',', $chart_users);
        $chart_income = [];
        $chart_income = implode(',', $chart_income);
        $members = User::where('referal_id', $user->id)->get();
        $wallet = Wallet::where('user_id', $user->id)->first();
        $orders = Order::where('user_id', $user->id)->get();
        $data = compact('members', 'wallet', 'orders', 'chart_users', 'user', 'chart_income');
        // dd($data);
        return view('frontend.inc.user.dashboard', $data);
    }

    public function profile(Request $request)
    {
        $user = Auth::guard('user')->user();
        $user = User::find($user->id);

        $request->replace($user->toArray());
        $request->flash();

        $data = compact('user');
        return view('frontend.inc.user.profile', $data);
    }
    public function editprofile(Request $request)
    {
        $user = Auth::guard('user')->user();
        $rules = [
            "name"      => "required",
            "aadhar_number" => "required|numeric|digits:12",
            "pan_number"     => "required|size:10",
        ];
        $request->validate($rules);

        $input = $request->except('_token', 'aadhar_front', 'aadhar_back', 'pan_image', 'password');
        $user->fill($input);
        $user->save();

        if ($request->hasFile('image')) {
            $file  = $request->file('image');
            $name  = 'IMG_' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/user/' . $name, (string) $image->encode());
            $user->update(['image' => $name]);
        }
        if ($request->hasFile('aadhar_front')) {
            $file  = $request->file('aadhar_front');
            $name  = 'IMG_front' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/aadharcard/' . $name, (string) $image->encode());
            $user->update(['aadhar_front' => $name]);
        }
        if ($request->hasFile('aadhar_back')) {
            $file  = $request->file('aadhar_back');
            $name  = 'IMG_back' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/aadharcard/' . $name, (string) $image->encode());
            $user->update(['aadhar_back' => $name]);
        }
        if ($request->hasFile('pan_image')) {
            $file  = $request->file('pan_image');
            $name  = 'IMG' . $user->id . '.' . $file->getClientOriginalExtension();

            $image = Image::make($file)->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put('public/pancard/' . $name, (string) $image->encode());
            $user->update(['pan_image' => $name]);
        }

        return redirect()->back()->with('success', 'Profile updated successfully.');
    }
    public function changepassword(Request $request)
    {
        $rules = [
            'oldpassword' => 'required',
            'newpassword' => 'required|string|min:6',
            'confirmpassword' => 'required|same:newpassword|min:6'
        ];

        $request->validate($rules);
        $user = Auth::guard('user')->user();
        if (!Hash::check($user->password, Hash::make($request->oldpassword))) {
            User::where('id', $user->id)->update(['password' => Hash::make($request->newpassword)]);
            return redirect()->back()->with('success', 'Your password has been changed!');
        } else {
            return redirect()->back()->with('error', 'Current password has been not correct.');
        }
    }
    public function product(Request $request)
    {
        $query = Product::where('status', true);
        if ($request->s) {
            $query->where('name', 'LIKE', '%' . $request->s . '%');
        }
        $lists = $query->get();
        $data = compact('lists');
        return view('frontend.inc.user.product', $data);
    }
    public function productdetail(Product $product)
    {
        $data = compact('product');
        return view('frontend.inc.user.productdetail', $data);
    }
    public function members(Request $request)
    {
        $user = Auth::guard('user')->user();
        $members = User::where('referal_id', $user->id)->get();
        $data = compact('members', 'user');
        return view('frontend.inc.user.members', $data);
    }
}
