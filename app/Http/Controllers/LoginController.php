<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends BaseController
{
    public function index()
    {
        $page = 'login';
        $title = 'Login';
        $keywords = 'Login';
        $description = 'Login';

        $data = compact('page', 'title', 'keywords', 'description');
        return view('frontend.guestlayout', $data);
    }

    public function checklogin(Request $request)
    {
        $rules = [
            "email"       => "required|email",
            "password"    => "required|string",
        ];
        $request->validate($rules);

        $crediencial = array(
            'email'     => $request->email,
            'password'  => $request->password
        );
        $is_remembered = !empty($request->remember_me) ? true : false;
        if (Auth::guard('user')->attempt($crediencial, $is_remembered)) {

            $user = Auth::guard('user')->user();
            // dd($user);
            return redirect(route('user.dashboard'))->with('success', 'You are successfully login.');
        } else {
            return redirect()->back()->with('error', 'Credentials not matched.');
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('user')->logout();
        return redirect(route('auth.login'))->with('success', 'You are successfully logout.');
    }
}
