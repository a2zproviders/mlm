<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $with = ['address', 'products', 'kit', 'user'];

    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id');
    }
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'order_id', 'id');
    }

    public function kit()
    {
        return $this->hasOne(Kit::class, 'id', 'kit_id');
    }
}
