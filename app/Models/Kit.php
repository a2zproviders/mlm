<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kit extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $with = ['products'];

    public function products()
    {
        return $this->hasMany(KitProduct::class, 'kit_id', 'id');
    }
}
