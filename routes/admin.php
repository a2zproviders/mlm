<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest:admin', 'namespace' => 'admin', 'as' => ''], function () {
    Route::any('login', 'LoginController@index')->name('login');
    Route::post('checklogin', 'LoginController@checklogin')->name('admin.checklogin');
});
Route::group(['middleware' => 'auth:admin', 'namespace' => 'admin', 'as' => 'admin.'], function () {
    Route::get('', 'HomeController@index')->name('dashboard');
    Route::get('logout', 'LoginController@logout')->name('logout');

    Route::get('settings', 'SettingController@index')->name('setting');
    Route::post('settings', 'SettingController@store')->name('setting.update');
    
    // User
    Route::get('user/delete/{user:id}', 'UserController@delete')->name('user.delete');
    Route::get('user/documentstatus/{user:id}', 'UserController@documentstatus')->name('user.documentstatus');
    Route::get('user/status/{user:id}', 'UserController@status')->name('user.status');
    Route::get('user/csv', 'UserController@csv')->name('user.csv');
    Route::get('user/pdf', 'UserController@pdf')->name('user.pdf');

    // Product
    Route::get('product/delete/{product:id}', 'ProductController@delete')->name('product.delete');
    Route::get('product/status/{product:id}', 'ProductController@status')->name('product.status');
    
    // Kit
    Route::get('kit/delete/{kit:id}', 'KitController@delete')->name('kit.delete');
    Route::get('kit/status/{kit:id}', 'KitController@status')->name('kit.status');
    Route::post('get_products_price', 'KitController@get_products_price')->name('get_products_price');

    // Faqs
    Route::get('faqs/delete/{faq:id}', 'FaqController@delete')->name('faqs.delete');

    // Faqs
    Route::get('page/delete/{page:id}', 'PageController@delete')->name('page.delete');

    // Blog Category
    Route::get('blogcategory/delete/{blogcategory:id}', 'BlogCategoryController@delete')->name('blogcategory.delete');
    
    // Blog
    Route::get('blog/delete/{blog:id}', 'BlogController@delete')->name('blog.delete');
    Route::get('blog/status/{blog:id}', 'BlogController@status')->name('blog.status');
    Route::get('blog/csv', 'BlogController@csv')->name('blog.csv');
    Route::get('blog/pdf', 'BlogController@pdf')->name('blog.pdf');

    Route::resources([
        'user' => UserController::class,
        'product' => ProductController::class,
        'kit' => KitController::class,
        'faqs' => FaqController::class,
        'page' => PageController::class,
        'blog' => BlogController::class,
        'blogcategory' => BlogCategoryController::class,
    ]);
    
    Route::get('order/delete/{order:id}', 'OrderController@delete')->name('order.delete');
    Route::get('order/show/{order:id}', 'OrderController@show')->name('order.show');
    Route::get('order/status/{order:id}', 'OrderController@status')->name('order.status');

    Route::get('order', 'OrderController@index')->name('order');
});
