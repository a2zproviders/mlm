<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::any('admin', function () {
    return false;
});

Route::group(['middleware' => 'guest', 'as' => ''], function () {
    Route::get('register/{kit:slug}', 'RegisterController@index')->name('auth.register');
    Route::post('register/{kit:slug}', 'RegisterController@store')->name('auth.register.store');
    Route::get('register/kyc/{id}/{kit:slug}', 'RegisterController@kyc')->name('auth.kyc');
    Route::post('register/kyc/{id}/{kit:slug}', 'RegisterController@register')->name('auth.kyc.store');
    Route::get('login', 'LoginController@index')->name('auth.login');
    Route::post('checklogin', 'LoginController@checklogin')->name('auth.checklogin');

    Route::get('forgot', 'RegisterController@forgot')->name('auth.forgot');

    Route::group(['middleware' => 'guest', 'prefix' => 'ajex', 'as' => 'ajex.'], function () {
        Route::post('register/check', 'AjexController@check')->name('check');
    });

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('package', 'HomeController@kit')->name('kit');
    Route::get('package/{kit:slug}', 'HomeController@kitdetail')->name('kit.detail');
    Route::get('product', 'HomeController@product')->name('product');
    Route::get('product/{product:slug}', 'HomeController@productdetail')->name('product.detail');
    Route::get('blog', 'HomeController@blog')->name('blog');
    Route::get('blog/{blog:slug}', 'HomeController@blogdetail')->name('blog.detail');
    Route::get('termcondition', 'HomeController@termcondition')->name('termcondition');
});
Route::group(['middleware' => 'auth:user', 'prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('', 'UserController@index')->name('dashboard');
    Route::get('product', 'UserController@product')->name('product');
    Route::get('product/{product:slug}', 'UserController@productdetail')->name('product.detail');
    Route::get('profile', 'UserController@profile')->name('profile');
    Route::post('editprofile', 'UserController@editprofile')->name('editprofile');
    Route::post('changepassword', 'UserController@changepassword')->name('changepassword');
    Route::get('logout', 'LoginController@logout')->name('logout');

    Route::get('checkout/{kit:slug}', 'CheckoutController@index')->name('checkout');
    Route::post('checkout', 'CheckoutController@store')->name('checkout.store');

    Route::post('address/store', 'AddressController@store')->name('address.store');

    Route::get('members', 'UserController@members')->name('members');
    Route::get('order', 'OrderController@index')->name('order');
    Route::get('order/{order:id}', 'OrderController@show')->name('order.invoice');
    Route::post('checkout', 'CheckoutController@store')->name('checkout.store');
});
