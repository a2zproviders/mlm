<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('address_id');
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->unsignedBigInteger('kit_id');
            $table->foreign('kit_id')->references('id')->on('kits');
            $table->integer('invoice_no')->nullable();
            $table->string('financial_year')->nullable();
            $table->string('amount')->nullable();
            $table->string('delivary_charge')->nullable();
            $table->string('discount')->nullable();
            $table->string('total_amount')->nullable();
            $table->enum('payment_type', ['online', 'cash'])->default('online');
            $table->enum('payment_status', ['due', 'paid'])->default('due');
            $table->enum('status', ['pending', 'accepted', 'shipped', 'received', 'canceled'])->default('pending');
            $table->string('txn_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
