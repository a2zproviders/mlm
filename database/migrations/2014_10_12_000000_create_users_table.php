<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('mobile')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('otp')->nullable();
            $table->string('aadhar_number')->nullable()->unique();
            $table->string('pan_number')->nullable()->unique();
            $table->string('image')->nullable();
            $table->string('aadhar_front')->nullable();
            $table->string('aadhar_back')->nullable();
            $table->string('pan_image')->nullable();
            $table->unsignedBigInteger('role_id')->nullable();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->int('lavel')->nullable();
            $table->longText('bio')->nullable();
            $table->date('dateofbirth')->nullable();
            $table->enum('is_verified', ['true', 'false'])->default('false');
            $table->enum('is_document', ['pending', 'completed','completed'])->default('pending');
            $table->enum('status', ['true', 'false'])->default('true');
            $table->enum('device_type', ['ios', 'android'])->nullable();
            $table->string('device_id')->nullable();
            $table->string('fcm_id')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->unsignedBigInteger('referal_id')->nullable();
            $table->foreign('referal_id')->references('id')->on('users');
            $table->string('referal_code')->nullable();
            $table->string('accept_code')->nullable();
            $table->enum('termcondition', ['true', 'false'])->default('true');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
