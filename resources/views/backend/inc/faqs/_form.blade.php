@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
    {{$message}}
</div>
@endif
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    {{$message}}
</div>
@endif
@if(count($errors->all()))
@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{$error}}
</div>
@endforeach
@endif
<div class="form-row">
    <div class="col-xl-12 mb-3">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Name','required'=>'required'])}}
        <div class="invalid-feedback">Please enter name.</div>
    </div>
    <div class="col-xl-12 mb-3">
        {{Form::label('description', 'Description')}}
        {{Form::textarea('description', '', ['class' => 'form-control', 'placeholder'=>'Description','rows'=>'4'])}}
        <div class="invalid-feedback">Please enter description.</div>
    </div>
</div>