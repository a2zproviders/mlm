@section('style')
<link href="{{ url('assets/plugins/multipleselect/multiple-select.css') }}" rel="stylesheet">
@stop
<div class="main-content app-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Add Kit</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.kit.index') }}">Kit</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Kit</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- ROW OPEN -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Add Kit</h3>
                        </div>
                        <div class="card-body">
                            {{ Form::open(['url' => route('admin.kit.store'), 'method'=>'POST', 'files' => true, 'class' => 'needs-validation','novalidate']) }}
                            <!-- <form method="post" action="{{ route('admin.kit.store') }}" files="true" class="needs-validation" novalidate=""> -->
                            {{ csrf_field() }}

                            @if($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                {{$message}}
                            </div>
                            @endif
                            @if($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                {{$message}}
                            </div>
                            @endif
                            @if(count($errors->all()))
                            @foreach($errors->all() as $error)
                            <div class="alert alert-danger">
                                {{$error}}
                            </div>
                            @endforeach
                            @endif
                            <div class="form-row">
                                <div class="col-xl-6 mb-3">
                                    {{Form::label('name', 'Name')}}
                                    {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Name','required'=>'required'])}}
                                    <div class="invalid-feedback">Please enter name.</div>
                                </div>
                                <div class="col-xl-6 mb-3">
                                    {{Form::label('slug', 'Slug')}}
                                    {{Form::text('slug', '', ['class' => 'form-control', 'placeholder'=>'Slug'])}}
                                    <div class="invalid-feedback">Please enter slug.</div>
                                </div>
                                <div class="col-xl-4 mb-3">
                                    {{Form::label('price', 'Price')}}
                                    {{Form::number('price', '', ['class' => 'form-control', 'placeholder'=>'Price','required'=>'required','min'=>'2999','max'=>'2999'])}}
                                    <div class="invalid-feedback">Please select product and selected products price equal to ₹2,999.</div>
                                </div>
                                <div class="col-xl-3 mb-3">
                                    {{Form::label('image', 'Image')}}
                                    {{Form::file('image',['class'=>'form-control','required'=>'required'])}}
                                    <div class="invalid-feedback">Please choose image.</div>
                                </div>
                                <div class="col-xl-5 mb-3">
                                    <label for="validationCustom05">Products</label>
                                    <select multiple="multiple" name="products[]" class="multi-select select-product" style="display: none;" data-url="{{ route('admin.get_products_price') }}" id="validationCustom05" required>
                                        <option selected="" disabled="" value="">Select Product</option>
                                        @foreach($productArr as $key=> $u)
                                        <option value="{{ $key }}">{{ $u }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">Please select product.</div>
                                </div>
                                <div class="col-xl-12 mb-3">
                                    {{Form::label('description', 'Description')}}
                                    {{Form::textarea('description', '', ['class' => 'form-control', 'placeholder'=>'Description','rows'=>'4'])}}
                                    <div class="invalid-feedback">Please enter description.</div>
                                </div>
                            </div>
                            <button class="btn btn-primary mt-5" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- ROW CLOSED -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>

@section('script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });
    $('.select-product').on('change', function() {
        console.log('dfkj');
        let url = $(this).data('url');
        let data = $(this).val();
        console.log('data', data);
        $.ajax({
            url: url,
            method: "POST",
            data: {
                'products': data,
            },
            success: function(res) {
                console.log('res', res);
                $('input[name=price]').val(res.price);
            },
        });
    });
</script>

<!-- Edit Table -->
<script src="{{ url('assets/plugins/multipleselect/multiple-select.js') }}"></script>
<script src="{{ url('assets/plugins/multipleselect/multi-select.js') }}"></script>
@stop