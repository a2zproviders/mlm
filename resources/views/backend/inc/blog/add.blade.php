@section('style')

@stop
<div class="main-content app-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Add Blog</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.product.index') }}">Blog</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Blog</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- ROW OPEN -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Add Blog</h3>
                        </div>
                        <div class="card-body">
                            {{ Form::open(['url' => route('admin.blog.store'), 'method'=>'POST', 'files' => true, 'class' => 'needs-validation','novalidate']) }}
                            {{ csrf_field() }}

                            @include('backend.inc.blog._form')
                            <button class="btn btn-primary mt-5" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- ROW CLOSED -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>

@section('script')

<script src="{{ url('assets/plugins/fancyuploader/jquery.fileupload.js') }}"></script>
<script src="{{ url('assets/plugins/fancyuploader/jquery.iframe-transport.js') }}"></script>
<script src="{{ url('assets/plugins/fancyuploader/jquery.fancy-fileupload.js') }}"></script>
<script src="{{ url('assets/plugins/fancyuploader/fancy-uploader.js') }}"></script>
@stop