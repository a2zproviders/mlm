@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
    {{$message}}
</div>
@endif
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    {{$message}}
</div>
@endif
@if(count($errors->all()))
@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{$error}}
</div>
@endforeach
@endif
<div class="form-row">
    <div class="col-xl-12 mb-3">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Name','required'=>'required'])}}
        <div class="invalid-feedback">Please enter name.</div>
    </div>
    <div class="col-xl-4 mb-3">
        {{Form::label('slug', 'Slug')}}
        {{Form::text('slug', '', ['class' => 'form-control', 'placeholder'=>'Slug'])}}
        <div class="invalid-feedback">Please enter slug.</div>
    </div>
    <div class="col-xl-4 mb-3">
        {{Form::label('category_id', 'Category')}}
        {{ Form::select('category_id', $categoryArr, '', ['class' => 'form-control select2','required'=>'required']) }}
        <div class="invalid-feedback">Please select category.</div>
    </div>
    <div class="col-xl-4 mb-3">
        {{Form::label('image', 'Image')}}
        {{Form::file('image',['class'=>'form-control'])}}
        <div class="invalid-feedback">Please choose image.</div>
    </div>

    <div class="col-xl-12 mb-3">
        {{Form::label('excerpt', 'Short Description')}}
        {{Form::textarea('excerpt', '', ['class' => 'form-control', 'placeholder'=>'Short Description','rows'=>'3'])}}
        <div class="invalid-feedback">Please enter short description.</div>
    </div>
    <div class="col-xl-12 mb-3">
        {{Form::label('description', 'Description')}}
        {{Form::textarea('description', '', ['class' => 'form-control', 'placeholder'=>'Description','rows'=>'6'])}}
        <div class="invalid-feedback">Please enter description.</div>
    </div>
</div>