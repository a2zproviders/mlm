<div class="main-content app-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Kit Details</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.kit.index') }}">Kit</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Kit Details</li>
                    </ol>
                </div>
            </div> <!-- PAGE-HEADER END -->
            <!-- ROW-1 OPEN -->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row row-sm">
                                <div class="col-xl-5 col-lg-12 col-md-12">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <img src="{{ url('storage/kit',$kit->image)}}" alt="img" class="img-fluid mx-auto d-block">
                                        </div>
                                    </div>
                                </div>
                                <div class="details col-xl-7 col-lg-12 col-md-12 mt-4 mt-xl-0">
                                    <div class="mt-2 mb-4">
                                        <h3 class="mb-3" style="text-transform: capitalize;">{{ $kit->name }}</h3>
                                        <p>{{ $kit->description }}</p>
                                        <h3 class="mb-4">
                                            <span class="me-2 fw-bold fs-25">₹ {{ $kit->price }} /- </span>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12">
                    <div class="card productdesc">
                        <div class="card-header">
                            <h3 class="card-title">Products</h3>
                        </div>
                        <div class="card-body">
                            <div id="responsive-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table class="table table-bordered text-nowrap border-bottom dataTable no-footer" id="responsive-datatable" role="grid" aria-describedby="responsive-datatable_info">
                                            <thead>
                                                <tr role="row">
                                                    <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">S. No.</th>
                                                    <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Name</th>
                                                    <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Image</th>
                                                    <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Price</th>
                                                    <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Short Description</th>
                                                    <th class="wd-25p border-bottom-0" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($kit->products as $key => $list)
                                                @php
                                                $sn = $key + 1;
                                                @endphp
                                                <tr>
                                                    <td>{{ $sn }}.</td>
                                                    <td>{{ $list->product->name }}</td>
                                                    <td>
                                                        <div class="d-flex">
                                                            @foreach($list->product->images as $key => $img)
                                                            <span class="avatar bradius" style="background-image: url('{{ url('storage/product',$img->image)}}');"></span>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                    <td>₹ {{ $list->product->price }} /-</td>
                                                    <td style="max-width: 300px; overflow: hidden; text-overflow: ellipsis;">{{ $list->product->excerpt }}</td>
                                                    <td>
                                                        <div class="btn-list">

                                                            <a href="{{ route('admin.product.show',$list->product->id) }}" class="btn btn-sm btn-warning">
                                                                <span class="fe fe-eye"> </span>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- ROW-1 CLOSED -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>