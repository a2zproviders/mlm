@section('style')

<!-- Edit Table -->
<!-- <link href="{{ url('assets/plugins/edit-table/edit-table.css') }}" rel="stylesheet"> -->

<link href="{{ url('assets/plugins/datatable/css/buttons.bootstrap5.min.css') }}" rel="stylesheet">
<link href="{{ url('assets/plugins/datatable/responsive.bootstrap5.css') }}" rel="stylesheet" />

@stop
<div class="main-content app-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Order List</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Order</li>
                    </ol>
                </div>
            </div> <!-- PAGE-HEADER END -->
            <!-- Row -->
            <div class="row row-sm">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Order List</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                @if($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    {{$message}}
                                </div>
                                @endif
                                @if($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    {{$message}}
                                </div>
                                @endif
                                <div id="responsive-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered text-nowrap border-bottom dataTable no-footer" id="responsive-datatable" role="grid" aria-describedby="responsive-datatable_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">S. No.</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Invoice No.</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">User</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Total Amount</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Payment Type</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Payment Status</th>
                                                        <th class="wd-15p border-bottom-0 sorting" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Txn Id</th>
                                                        <th class="wd-25p border-bottom-0 sorting" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Created at</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Status</th>
                                                        <th class="wd-25p border-bottom-0" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($lists as $key => $list)
                                                    <tr>
                                                        @if(method_exists($lists, 'links'))
                                                        <td>{{ $key + $lists->firstItem() }}.</td>
                                                        @else
                                                        <td>{{ $key + 1 }}.</td>
                                                        @endif
                                                        <td><a href="{{ route('admin.order.show',$list->id) }}" title="view details"><i class="fe fe-eye"> </i> {{ sprintf("%s/%s/%04d", 'MLM', $list->financial_year, $list->invoice_no) }}</a></td>
                                                        <td>{{ $list->user_id }}</td>
                                                        <td>₹ {{ $list->total_amount }} /-</td>
                                                        <td>{{ $list->payment_type }}</td>
                                                        <td>{{ $list->payment_status }}</td>
                                                        <td>{{ $list->txn_id }}</td>
                                                        <td>{{ $list->created_at }}</td>
                                                        <td>
                                                            <div class="mt-sm-1 d-block">
                                                                <select class="form-control" id="order_status" data-url="{{ route('admin.order.status',$list->id) }}">
                                                                    <option value="pending" @if($list->status == 'pending') selected @endif>
                                                                        <a href="#">
                                                                            <span class="badge bg-success-transparent rounded-pill text-success p-2 px-3">Pending</span>
                                                                        </a>
                                                                    </option>
                                                                    <option value="accepted" @if($list->status == 'accepted') selected @endif>
                                                                        <a href="#">
                                                                            <span class="badge bg-warning-transparent rounded-pill text-warning p-2 px-3">Accepted</span>
                                                                        </a>
                                                                    </option>
                                                                    <option value="shipped" @if($list->status == 'shipped') selected @endif>
                                                                        <a href="#">
                                                                            <span class="badge bg-warning-transparent rounded-pill text-warning p-2 px-3">Shipped</span>
                                                                        </a>
                                                                    </option>
                                                                    <option value="received" @if($list->status == 'received') selected @endif>
                                                                        <a href="#">
                                                                            <span class="badge bg-warning-transparent rounded-pill text-warning p-2 px-3">Received</span>
                                                                        </a>
                                                                    </option>
                                                                    <option value="canceled" @if($list->status == 'canceled') selected @endif>
                                                                        <a href="#">
                                                                            <span class="badge bg-warning-transparent rounded-pill text-warning p-2 px-3">Canceled</span>
                                                                        </a>
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="btn-list">
                                                                <!-- <a href="{{ route('admin.user.edit',$list->id) }}" class="btn btn-sm btn-primary">
                                                                    <span class="fe fe-edit"> </span>
                                                                </a> -->
                                                                <a href="#" data-url="{{ route('admin.order.delete',$list->id) }}" id="single_delete" class="btn btn-sm btn-danger">
                                                                    <span class="fe fe-trash-2"> </span>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        @if(method_exists($lists, 'links'))
                                        @include('frontend.templete.pagination', ['paginator' => $lists,'link_limit'=>4])
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>
@section('script')

<!-- Edit Table -->
<script src="{{ url('assets/plugins/edit-table/edit-table.js') }}"></script>
<script src="{{ url('assets/plugins/edit-table/bst-edittable.js') }}"></script>

<script src="{{ url('assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/dataTables.bootstrap5.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/jszip.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/buttons.html5.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/buttons.print.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/buttons.colVis.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/responsive.bootstrap5.min.js') }}"></script>
<script src="{{ url('assets/js/table-data.js') }}"></script>

@stop