@section('style')
<link href="{{ url('assets/plugins/multipleselect/multiple-select.css') }}" rel="stylesheet">
@stop
<div class="main-content app-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Edit Blog Category</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.blogcategory.index') }}">Blog Category</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Blog Category</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- ROW OPEN -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Edit Blog Category</h3>
                        </div>
                        <div class="card-body">
                            {{ Form::open(['url' => route('admin.blogcategory.update',$blogcategory->id), 'method'=>'PUT', 'files' => true, 'class' => 'needs-validation','novalidate']) }}
                            {{ csrf_field() }}

                            @include('backend.inc.blogcategory._form')
                            <button class="btn btn-primary mt-5" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- ROW CLOSED -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>


@section('script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });
    $('.select-product').on('change', function() {
        console.log('dfkj');
        let url = $(this).data('url');
        let data = $(this).val();
        console.log('data', data);
        $.ajax({
            url: url,
            method: "POST",
            data: {
                'products': data,
            },
            success: function(res) {
                console.log('res', res);
                $('input[name=price]').val(res.price);
            },
        });
    });
</script>

<!-- Edit Table -->
<script src="{{ url('assets/plugins/multipleselect/multiple-select.js') }}"></script>
<script src="{{ url('assets/plugins/multipleselect/multi-select.js') }}"></script>
@stop