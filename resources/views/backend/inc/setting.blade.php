<div class="main-content app-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Settings</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Settings</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- ROW OPEN -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Settings</h3>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ route('admin.setting.update') }}" class="needs-validation" novalidate="">
                                {{ csrf_field() }}

                                @if($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    {{$message}}
                                </div>
                                @endif
                                @if($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    {{$message}}
                                </div>
                                @endif
                                @if(count($errors->all()))
                                @foreach($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{$error}}
                                </div>
                                @endforeach
                                @endif
                                <div class="form-row">
                                    <div class="col-xl-3 mb-3">
                                        {{Form::label('site_name', 'Site Name')}}
                                        {{Form::text('site_name', '', ['class' => 'form-control', 'placeholder'=>'Site Name','required'=>'required'])}}
                                        <div class="invalid-feedback">Please enter site name.</div>
                                    </div>
                                    <div class="col-xl-3 mb-3">
                                        <label for="validationCustom02">Tagline</label>
                                        <input type="text" class="form-control" id="validationCustom02" placeholder="Tagline">
                                        <div class="invalid-feedback">Please enter tagline.</div>
                                    </div>
                                    <div class="col-xl-3 mb-3">
                                        {{Form::label('email', 'Email')}}
                                        {{Form::email('email', '', ['class' => 'form-control', 'placeholder'=>'Email','required'=>'required'])}}
                                        <div class="invalid-feedback">Please enter email.</div>
                                    </div>
                                    <div class="col-xl-3 mb-3">
                                        {{Form::label('mobile', 'Mobile')}}
                                        {{Form::text('mobile', '', ['class' => 'form-control', 'placeholder'=>'Mobile'])}}
                                        <div class="invalid-feedback">Please enter mobile.</div>
                                    </div>
                                    <div class="col-xl-6 mb-3">
                                        {{Form::label('address', 'Address')}}
                                        {{Form::textarea('address', '', ['class' => 'form-control', 'placeholder'=>'Address','rows'=>'3'])}}
                                        <div class="invalid-feedback">Please enter address.</div>
                                    </div>
                                </div>
                                <button class="btn btn-primary mt-5" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- ROW CLOSED -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>
@section('script')

@stop