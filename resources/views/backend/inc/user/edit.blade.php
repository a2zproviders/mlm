<div class="main-content app-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Edit User</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.user.index') }}">User</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- ROW OPEN -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Edit User</h3>
                        </div>
                        <div class="card-body">
                            {{ Form::open(['url' => route('admin.user.update',$user->id), 'method'=>'PUT', 'files' => true, 'class' => 'needs-validation','novalidate']) }}
                            <!-- <form method="post" action="{{ route('admin.user.store') }}" files="true" class="needs-validation" novalidate=""> -->
                            {{ csrf_field() }}

                            @if($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                {{$message}}
                            </div>
                            @endif
                            @if($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                {{$message}}
                            </div>
                            @endif
                            @if(count($errors->all()))
                            @foreach($errors->all() as $error)
                            <div class="alert alert-danger">
                                {{$error}}
                            </div>
                            @endforeach
                            @endif
                            <div class="form-row">
                                <div class="col-xl-4 mb-3">
                                    {{Form::label('name', 'Name')}}
                                    {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Name','required'=>'required'])}}
                                    <div class="invalid-feedback">Please enter name.</div>
                                </div>
                                <div class="col-xl-4 mb-3">
                                    {{Form::label('email', 'Email')}}
                                    {{Form::email('email', '', ['class' => 'form-control', 'placeholder'=>'Email','required'=>'required'])}}
                                    <div class="invalid-feedback">Please enter valid email.</div>
                                </div>
                                <div class="col-xl-4 mb-3">
                                    {{Form::label('password', 'Password')}}
                                    @if(@!$user)
                                    {{Form::input('password','password', '', ['class' => 'form-control', 'placeholder'=>'Password','required'=>'required','minlength'=>'6'])}}
                                    @else
                                    {{Form::input('password','password', '', ['class' => 'form-control', 'placeholder'=>'Password'])}}
                                    @endif
                                    <div class="invalid-feedback">Please enter valid password.</div>
                                </div>
                                <div class="col-xl-3 mb-3">
                                    {{Form::label('mobile', 'Mobile')}}
                                    {{Form::text('mobile', '', ['class' => 'form-control', 'placeholder'=>'Mobile','required'=>'required','minlength'=>'10','maxlength'=>'12'])}}
                                    <div class="invalid-feedback">Please enter valid mobile number.</div>
                                </div>
                                <div class="col-xl-3 mb-3">
                                    {{Form::label('aadhar_number', 'Aadhar Number')}}
                                    {{Form::text('aadhar_number', '', ['class' => 'form-control', 'placeholder'=>'Aadhar Number','required'=>'required','minlength'=>'12','maxlength'=>'12'])}}
                                    <div class="invalid-feedback">Please enter valid aadhar number.</div>
                                </div>
                                <div class="col-xl-3 mb-3">
                                    {{Form::label('aadhar_front', 'Aadhar Front Image')}}
                                    @if(@!$user->aadhar_front)
                                    {{Form::file('aadhar_front',['class'=>'form-control','required'=>'required'])}}
                                    @else
                                    {{Form::file('aadhar_front',['class'=>'form-control'])}}
                                    @endif
                                    <div class="invalid-feedback">Please choose aadhar front image.</div>
                                </div>
                                <div class="col-xl-3 mb-3">
                                    {{Form::label('aadhar_back', 'Aadhar Back Image')}}
                                    @if(@!$user->aadhar_back)
                                    {{Form::file('aadhar_back',['class'=>'form-control','required'=>'required'])}}
                                    @else
                                    {{Form::file('aadhar_back',['class'=>'form-control'])}}
                                    @endif
                                    <div class="invalid-feedback">Please choose aadhar back image.</div>
                                </div>
                                <div class="col-xl-4 mb-3">
                                    {{Form::label('image', 'Image')}}
                                    @if(@!$user->image)
                                    {{Form::file('image',['class'=>'form-control','required'=>'required'])}}
                                    @else
                                    {{Form::file('image',['class'=>'form-control'])}}
                                    @endif
                                    <div class="invalid-feedback">Please choose image.</div>
                                </div>
                                <div class="col-xl-4 mb-3">
                                    {{Form::label('pan_number', 'Pancard Number')}}
                                    {{Form::text('pan_number', '', ['class' => 'form-control', 'placeholder'=>'Pancard Number','required'=>'required','minlength'=>'10','maxlength'=>'10','id'=>'pancard_number'])}}
                                    <div class="invalid-feedback">Please enter valid pancard number.</div>
                                </div>
                                <div class="col-xl-4 mb-3">
                                    {{Form::label('pan_image', 'Pancard Image')}}
                                    @if(@!$user->pan_image)
                                    {{Form::file('pan_image',['class'=>'form-control','required'=>'required'])}}
                                    @else
                                    {{Form::file('pan_image',['class'=>'form-control'])}}
                                    @endif
                                    <div class="invalid-feedback">Please choose pancard image.</div>
                                </div>
                            </div>
                            <!-- <div class="form-row">
                                    <div class="col-xl-6 mb-3"> <label for="validationCustom03">City</label> <input type="text" class="form-control" id="validationCustom03" required="">
                                        <div class="invalid-feedback">Please provide a valid city.</div>
                                    </div>
                                    <div class="col-xl-3 mb-3"> <label for="validationCustom04">State</label>
                                        <select class="form-control  select2" id="validationCustom04" required="">
                                            <option selected="" disabled="" value="">State</option>
                                            <option>New york</option>
                                            <option>New york</option>
                                            <option>New york</option>
                                            <option>New york</option>
                                            <option>New york</option>
                                            <option>New york</option>
                                        </select>
                                        <div class="invalid-feedback">Please select a valid state.</div>
                                    </div>
                                    <div class="col-xl-3 mb-3"> <label for="validationCustom05">Zip</label>
                                        <input type="text" class="form-control" id="validationCustom05" required="">
                                        <div class="invalid-feedback">Please provide a valid zip.</div>
                                    </div>
                                </div> -->
                            <button class="btn btn-primary mt-5" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- ROW CLOSED -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>