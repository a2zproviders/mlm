@section('style')

@stop
<div class="main-content app-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">User List</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">User</li>
                    </ol>
                </div>
            </div> <!-- PAGE-HEADER END -->
            <!-- Row -->
            <div class="row row-sm">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">User List</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                @if($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    {{$message}}
                                </div>
                                @endif
                                @if($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    {{$message}}
                                </div>
                                @endif
                                <form class="needs-validation">
                                    <div class="d-flex">
                                        <div style="display: flex; align-items: center; margin-right: 10px; min-width: 100px;">
                                            <label for="limit" style="margin-right: 5px;">Limit</label>
                                            <select name="limit" class="form-control select2" id="limit" style="margin-right: 10px; min-width: 60px;">
                                                <option value="10" @if(@$query['limit']==10) selected @endif>10</option>
                                                <option value="50" @if(@$query['limit']==50) selected @endif>50</option>
                                                <option value="100" @if(@$query['limit']==100) selected @endif>100</option>
                                            </select>
                                        </div>
                                        <div style="margin-right: 10px;">
                                            <input type="text" name="s" value="{{ @$query['s'] }}" class="form-control" placeholder="Search..." style="min-width: 100px;" />
                                        </div>
                                        <div style="margin-right: 10px;">
                                            <select name="user" class="form-control select2" style="margin-right: 20px; min-width: 100px;">
                                                <option value=""> Select User </option>
                                                @foreach($users as $cat)
                                                <option value="{{ $cat->id }}" @if(@$query['user']==$cat->id) selected @endif>{{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div style="margin-right: 10px;">
                                            <select name="status" class="form-control select2" style="margin-right: 20px; min-width: 100px;">
                                                <option value="">Select Status</option>
                                                <option value="true" @if(@$query['status']=='true' ) selected @endif>True</option>
                                                <option value="false" @if(@$query['status']=='false' ) selected @endif>False</option>
                                            </select>
                                        </div>
                                        <div style="margin-right: 10px;"><Button class="btn btn-primary">Submit</Button></div>
                                        <div style="margin-right: 10px;"><a href="{{ route('admin.user.index') }}" class="btn btn-danger" style="background-color: #e74c3c;">Clear All</a></div>
                                        <div style="margin-right: 10px;"><a href="{{ route('admin.user.csv',@$query) }}" class="btn btn-danger" style="background-color: #e74c3c;">Export CSV</a></div>
                                        <div style="margin-right: 10px;"><a href="{{ route('admin.user.pdf',@$query) }}" target="_blank" class="btn btn-danger" style="background-color: #e74c3c;">Export PDF</a></div>
                                    </div>
                                </form>
                                <table class="table border text-nowrap text-md-nowrap table-bordered mb-0 mt-5">
                                    <thead>
                                        <tr>
                                            <th>S. No.</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Aadhar card</th>
                                            <th>Pancard</th>
                                            <th>Referal By</th>
                                            <th>Lavel</th>
                                            <th>Code</th>
                                            <th>Created at</th>
                                            <th>Document Status</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $sn = $lists->firstItem();
                                        @endphp
                                        @foreach($lists as $key => $list)
                                        <tr>
                                            <td>{{ $sn + $key }}.</td>
                                            <td>
                                                <div class="d-flex">
                                                    <span class="avatar bradius" style="background-image: url('{{ url('storage/user',$list->image)}}');"></span>
                                                    <div class="ms-3 mt-0 mt-sm-2 d-block">
                                                        <h6 class="mb-0 fs-14 fw-semibold"> {{ $list->name }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><a href="mailto:{{ $list->email }}">{{ $list->email }}</a></td>
                                            <td><a href="tel:{{ $list->mobile }}">{{ $list->mobile }}</a></td>
                                            <td>
                                                <div class="d-flex">
                                                    <span class="avatar bradius" style="background-image: url('{{ url('storage/aadharcard',$list->aadhar_front)}}');"></span>
                                                    <span class="avatar bradius" style="background-image: url('{{ url('storage/aadharcard',$list->aadhar_back)}}');"></span>
                                                    <div class="ms-3 mt-0 mt-sm-2 d-block">
                                                        <h6 class="mb-0 fs-14 fw-semibold"> {{ $list->aadhar_number }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <span class="avatar bradius" style="background-image: url('{{ url('storage/pancard',$list->pan_image)}}');"></span>
                                                    <div class="ms-3 mt-0 mt-sm-2 d-block">
                                                        <h6 class="mb-0 fs-14 fw-semibold"> {{ $list->pan_number }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{ $list->referal_id ? $list->referal_user->name : 'N/A' }}</td>
                                            <td>Lavel {{ $list->lavel }}</td>
                                            <td>{{ $list->referal_code }}</td>
                                            <td>{{ $list->created_at }}</td>
                                            <td>
                                                <div class="mt-sm-1 d-block">
                                                    @if($list->is_document == 'completed')
                                                    <a href="#" data-url="{{ route('admin.user.documentstatus',$list->id) }}" id="single_status">
                                                        <span class="badge bg-success-transparent rounded-pill text-success p-2 px-3">Completed</span>
                                                    </a>
                                                    @else
                                                    <a href="#" data-url="{{ route('admin.user.documentstatus',$list->id) }}" id="single_status">
                                                        <span class="badge bg-warning-transparent rounded-pill text-warning p-2 px-3">Pending</span>
                                                    </a>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="mt-sm-1 d-block">
                                                    @if($list->status == 'true')
                                                    <a href="#" data-url="{{ route('admin.user.status',$list->id) }}" id="single_status">
                                                        <span class="badge bg-success-transparent rounded-pill text-success p-2 px-3">True</span>
                                                    </a>
                                                    @else
                                                    <a href="#" data-url="{{ route('admin.user.status',$list->id) }}" id="single_status">
                                                        <span class="badge bg-danger-transparent rounded-pill text-warning p-2 px-3">False</span>
                                                    </a>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="btn-list">

                                                    <a href="{{ route('admin.user.edit',$list->id) }}" class="btn btn-sm btn-primary">
                                                        <span class="fe fe-edit"> </span>
                                                    </a>
                                                    <a href="#" data-url="{{ route('admin.user.delete',$list->id) }}" id="single_delete" class="btn btn-sm btn-danger">
                                                        <span class="fe fe-trash-2"> </span>
                                                    </a>
                                                    <a href="{{ route('admin.user.show',$list->id) }}" class="btn btn-sm btn-warning">
                                                        <span class="fe fe-eye"> </span>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-3">
                                @if(method_exists($lists, 'links'))
                                @include('frontend.templete.pagination', ['paginator' => $lists,'query'=>$query,'link_limit'=>5])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>
@section('script')


@stop