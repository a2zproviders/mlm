@section('style')

@stop
<div class="main-content app-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Add Product</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.product.index') }}">Product</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Product</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- ROW OPEN -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Add Product</h3>
                        </div>
                        <div class="card-body">
                            {{ Form::open(['url' => route('admin.product.store'), 'method'=>'POST', 'files' => true, 'class' => 'needs-validation','novalidate']) }}
                            {{ csrf_field() }}

                            @if($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                {{$message}}
                            </div>
                            @endif
                            @if($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                {{$message}}
                            </div>
                            @endif
                            @if(count($errors->all()))
                            @foreach($errors->all() as $error)
                            <div class="alert alert-danger">
                                {{$error}}
                            </div>
                            @endforeach
                            @endif
                            <div class="form-row">
                                <div class="col-xl-12 mb-3">
                                    {{Form::label('name', 'Name')}}
                                    {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Name','required'=>'required'])}}
                                    <div class="invalid-feedback">Please enter name.</div>
                                </div>
                                <div class="col-xl-4 mb-3">
                                    {{Form::label('slug', 'Slug')}}
                                    {{Form::text('slug', '', ['class' => 'form-control', 'placeholder'=>'Slug'])}}
                                    <div class="invalid-feedback">Please enter slug.</div>
                                </div>
                                <div class="col-xl-4 mb-3">
                                    {{Form::label('price', 'Price')}}
                                    {{Form::number('price', '', ['class' => 'form-control', 'placeholder'=>'Price','required'=>'required'])}}
                                    <div class="invalid-feedback">Please enter price.</div>
                                </div>
                                <div class="col-xl-4 mb-3">
                                    {{Form::label('image', 'Image')}}
                                    {{Form::file('image',['class'=>'form-control','required'=>'required'])}}
                                    <div class="invalid-feedback">Please choose image.</div>
                                </div>

                                <div class="col-xl-12 mb-3">
                                    {{Form::label('excerpt', 'Short Description')}}
                                    {{Form::textarea('excerpt', '', ['class' => 'form-control', 'placeholder'=>'Short Description','rows'=>'3'])}}
                                    <div class="invalid-feedback">Please enter short description.</div>
                                </div>
                                <div class="col-xl-12 mb-3">
                                    {{Form::label('description', 'Description')}}
                                    {{Form::textarea('description', '', ['class' => 'form-control', 'placeholder'=>'Description','rows'=>'6'])}}
                                    <div class="invalid-feedback">Please enter description.</div>
                                </div>
                                <!-- <div class="col-xl-12 mb-3">
                                    {{Form::label('description', 'Upload Images')}}
                                    <input id="demo" type="file" name="files" accept=".jpg, .png, image/jpeg, image/png" multiple="" class="ff_fileupload_hidden">
                                </div> -->
                            </div>
                            <button class="btn btn-primary mt-5" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- ROW CLOSED -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>

@section('script')

<script src="{{ url('assets/plugins/fancyuploader/jquery.fileupload.js') }}"></script>
<script src="{{ url('assets/plugins/fancyuploader/jquery.iframe-transport.js') }}"></script>
<script src="{{ url('assets/plugins/fancyuploader/jquery.fancy-fileupload.js') }}"></script>
<script src="{{ url('assets/plugins/fancyuploader/fancy-uploader.js') }}"></script>
@stop