<!DOCTYPE html>
<html>

<head>
    <title>Blogs</title>
</head>
<style>
    table {
        width: 100%;
    }

    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 5px;
    }

    th {
        text-align: left;
    }

    h3 {
        text-align: center;
    }
</style>

<body>
    <h3>Users</h3>
    <table>
        <thead>
            <tr>
                <th>Detail</th>
                <!-- <th>Email</th> -->
                <!-- <th>Mobile</th> -->
                <th>Aadhar or Pan Number</th>
                <!-- <th>Pan Number</th> -->
                <th>Referal By</th>
                <th>Code</th>
                <th>Lavel</th>
                <!-- <th>Bio</th> -->
                <th>Created At</th>
                <th>Document Status</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($lists as $list)
            <tr>
                <td>{{ $list->name }}<br>{{ $list->email }}<br>{{ $list->mobile }}</td>
                <!-- <td>{{ $list->email }}</td> -->
                <!-- <td>{{ $list->mobile }}</td> -->
                <td>{{ $list->aadhar_number }} <br> {{ $list->pan_number }}</td>
                <!-- <td>{{ $list->pan_number }}</td> -->
                <td>{{ $list->referal_id }}</td>
                <td>{{ $list->referal_code }}</td>
                <td>Lavel {{ $list->lavel }}</td>
                <!-- <td>{{ $list->bio }}</td> -->
                <td>{{ $list->created_at }}</td>
                <td>{{ $list->is_document }}</td>
                <td>{{ $list->status }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>