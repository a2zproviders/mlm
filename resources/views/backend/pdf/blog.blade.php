<!DOCTYPE html>
<html>

<head>
    <title>Blogs</title>
</head>
<style>
    table {
        width: 100%;
    }

    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 5px;
    }

    th {
        text-align: left;
    }

    h3 {
        text-align: center;
    }
</style>

<body>
    <h3>Blogs</h3>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Category</th>
                <th>Excerpt</th>
                <th>Descritpion</th>
                <th>Created At</th>
            </tr>
        </thead>
        <tbody>
            @foreach($blogs as $blog)
            <tr>
                <td>{{ $blog->name }}</td>
                <td>{{ $blog->category->name }}</td>
                <td>{{ $blog->excerpt }}</td>
                <td>{{ $blog->description }}</td>
                <td>{{ $blog->created_at }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>