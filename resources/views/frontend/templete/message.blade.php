@if($errors)
@foreach($errors as $error)
<div class="alert alert-danger alert-block">
    {{$error}}
</div>
@endforeach
<!-- <div class="alert alert-success alert-block"></div> -->
@endif