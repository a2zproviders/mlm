@extends('frontend.layout.master')
@section('title','Kits')
@section('keywords','Kits Keywords')
@section('description','Kits Description')
@section('contant')
@if(!$lists)
<!-- GLOBAL-LOADER -->
<div id="page-loader"> <img src="{{ url('assets/images/loader.svg') }}" class="loader-img" alt="Loader"> </div> 
<!-- GLOBAL-LOADER -->
@else
<div class="main-content hor-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Kit</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Kit</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- ROW-1 OPEN -->
            <div class="row row-cards mt-5">
                <div class="col-xl-3 col-lg-4">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <form>
                                        <div class="input-group d-flex w-100 float-start">
                                            <input type="text" name="s" class="form-control border-end-0 my-2" placeholder="Search ...">
                                            <button class="btn input-group-text bg-transparent border-start-0 text-muted my-2">
                                                <i class="fe fe-search text-muted" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @if($popular_kits->count())
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">Top Kits</div>
                                </div>
                                <div class="card-body" style="min-height: 40vh;">
                                    <div class="">
                                        @foreach($popular_kits as $key => $kit)
                                        <div class="d-flex overflow-visible @if($key>1) mt-5 @endif"> <img class="avatar bradius avatar-xl me-4 p-2 bg-white border" src="{{ url('assets/images/pngs/4.jpg') }}" alt="avatar-img">
                                            <div class="media-body valign-middle oneline-ellipsis"> <a href="{{ route('kit.detail',$kit->slug) }}" class="fw-semibold text-dark" style="text-transform: capitalize;">{{ $kit->name }}</a>
                                                <p class="p-0 m-0 oneline-ellipsis">{{ $kit->description }}</p>
                                                <!-- <div class="mb-1 text-warning"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i> </div> -->
                                                <h5 class="fw-bold">₹{{ $kit->price }} /-</h5>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div> <!-- COL-END -->
                <div class="col-xl-9 col-lg-8">
                    <div class="row">
                        @if($lists->count())
                        @foreach($lists as $list)
                        <div class="col-md-6 col-xl-4">
                            <div class="card">
                                <div class="product-grid6">
                                    <div class="product-image6 p-5">
                                        <a href="{{ route('kit.detail',$list->slug) }}" class="bg-light">
                                            <img class="img-fluid br-7 w-100 product-height" src="{{ url('storage/kit',$list->image) }}" alt="img">
                                        </a>
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="product-content text-center">
                                            <h1 class="title fw-bold fs-20"><a href="{{ route('kit.detail',$list->slug) }}">{{ $list->name }}</a></h1>
                                            <!-- <div class="mb-2 text-warning"> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star-half-o text-warning"></i> <i class="fa fa-star-o text-warning"></i> </div> -->
                                            <div class="price">₹{{ $list->price }} /-
                                                <!-- <span class="ms-4">$799</span>  -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-center">
                                        @if(auth()->guard('user')->user())
                                        <a href="{{ route('user.checkout',$kit->slug) }}" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart me-2"></i>Buy Now</a>
                                        @else
                                        <a href="{{ route('auth.register',$kit->slug) }}" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart me-2"></i>Buy Now</a>
                                        @endif
                                        <!-- <a href="#" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart me-2"></i>Buy Now</a> -->
                                        <!-- <a href="#" class="btn btn-outline-primary mb-1"><i class="fe fe-heart me-2 wishlist-icon"></i>Add to wishlist</a>  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="card">
                            <div class="card-body">
                                <div class="text-center">No records found.</div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div> <!-- COL-END -->
            </div> <!-- ROW-1 CLOSED -->
        </div> <!-- ROW-1 END -->
    </div> <!-- CONTAINER CLOSED -->
</div>
</div>
@endif

@stop