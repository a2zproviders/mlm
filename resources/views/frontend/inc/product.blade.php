@extends('frontend.layout.master')
@section('title','Products')
@section('keywords','Products Keywords')
@section('description','Products Description')
@section('contant')

<div class="main-content hor-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container">
            <!-- PAGE-HEADER -->
            <!-- <div class="page-header">
                <h1 class="page-title">Shop</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">E-Commerce</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Shop</li>
                    </ol>
                </div>
            </div>  -->
            <!-- PAGE-HEADER END -->
            <!-- ROW-1 OPEN -->
            <div class="row row-cards mt-5">
                <div class="col-xl-3 col-lg-4">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <form>
                                        <div class="input-group d-flex w-100 float-start"> <input type="text" name="s" class="form-control border-end-0 my-2" placeholder="Search ..."> <button class="btn input-group-text bg-transparent border-start-0 text-muted my-2"> <i class="fe fe-search text-muted" aria-hidden="true"></i> </button> </div>
                                    </form>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">Top Products</div>
                                </div>
                                <div class="card-body">
                                    <div class="">
                                        <div class="d-flex overflow-visible"> <img class="avatar bradius avatar-xl me-4 p-2 bg-white border" src="{{ url('assets/images/pngs/4.jpg') }}" alt="avatar-img">
                                            <div class="media-body valign-middle"> <a href="" class="fw-semibold text-dark">Hand Bag</a>
                                                <!-- <div class="mb-1 text-warning"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i> </div> -->
                                                <h5 class="fw-bold">$345</h5>
                                            </div>
                                        </div>
                                        <div class="d-flex overflow-visible mt-5"> <img class="avatar bradius avatar-xl me-4 p-2 bg-white border" src="{{ url('assets/images/pngs/4.jpg') }}" alt="avatar-img">
                                            <div class="media-body valign-middle"> <a href="" class="fw-semibold text-dark">Chair</a>
                                                <!-- <div class="mb-1 text-warning"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i> </div> -->
                                                <h5 class="fw-bold">$345</h5>
                                            </div>
                                        </div>
                                        <div class="d-flex overflow-visible mt-5"> <img class="avatar bradius avatar-xl me-4 p-2 bg-white border" src="{{ url('assets/images/pngs/4.jpg') }}" alt="avatar-img">
                                            <div class="media-body valign-middle"> <a href="" class="fw-semibold text-dark">Laptop Bag</a>
                                                <!-- <div class="mb-1 text-warning"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i> </div> -->
                                                <h5 class="fw-bold">$345</h5>
                                            </div>
                                        </div>
                                        <div class="d-flex overflow-visible mt-5"> <img class="avatar bradius avatar-xl me-4 p-2 bg-white border" src="{{ url('assets/images/pngs/4.jpg') }}" alt="avatar-img">
                                            <div class="media-body valign-middle"> <a href="" class="fw-semibold text-dark">Watch</a>
                                                <!-- <div class="mb-1 text-warning"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i> </div> -->
                                                <h5 class="fw-bold">$345</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- COL-END -->
                <div class="col-xl-9 col-lg-8">
                    <div class="row">
                        <div class="col-md-6 col-xl-4">
                            <div class="card">
                                <div class="product-grid6">
                                    <div class="product-image6 p-5">
                                        <a href="{{ route('product.detail','test-product') }}" class="bg-light"> <img class="img-fluid br-7 w-100" src="{{ url('assets/images/pngs/4.jpg') }}" alt="img"> </a>
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="product-content text-center">
                                            <h1 class="title fw-bold fs-20"><a href="{{ route('product.detail','test-product') }}">Candy Pure Rose Water</a></h1>
                                            <!-- <div class="mb-2 text-warning"> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star-half-o text-warning"></i> <i class="fa fa-star-o text-warning"></i> </div> -->
                                            <div class="price">₹ 599
                                                <!-- <span class="ms-4">$799</span>  -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-center">
                                        <a href="#" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart me-2"></i>Buy Now</a>
                                        <!-- <a href="#" class="btn btn-outline-primary mb-1"><i class="fe fe-heart me-2 wishlist-icon"></i>Add to wishlist</a>  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- COL-END -->
            </div> <!-- ROW-1 CLOSED -->
        </div> <!-- ROW-1 END -->
    </div> <!-- CONTAINER CLOSED -->
</div>
</div>

@stop