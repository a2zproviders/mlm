@extends('frontend.layout.usermaster')
@section('title', 'Order Invoice')
@section('keywords', 'Order Invoice Keywords')
@section('description', 'Order Invoice Description')
@section('contant')
@section('style')
<style>
    .table.invoice-header>:not(caption)>*>* {
        border-bottom-width: 0px;
    }
</style>
@stop
<div class="main-content app-content mt-0">
    <div class="side-app mt-5">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <!-- <div class="page-header">
                <h1 class="page-title">Invoice</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('user.order') }}">Orders</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Invoice</li>
                    </ol>
                </div>
            </div>  -->
            <!-- PAGE-HEADER END -->
            <!-- ROW-1 OPEN -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="pt-5">
                                <table class="table mb-0 text-nowrap p-0 invoice-header" style="color: #282f53; border-bottom-width: 0;">
                                    <tbody>
                                        <tr class="">
                                            <td class="p-0 m-0">
                                                <a class="header-brand" href="index.html">
                                                    <img src="{{ url('assets/images/brand/logo-3.png') }}" class="header-brand-img logo-3" alt="Sash logo">
                                                    <img src="{{ url('assets/images/brand/logo.png') }}" class="header-brand-img logo" alt="Sash logo">
                                                </a>
                                                <div>
                                                    <address class="pt-3"> {{ $setting->address }}<br>{{ $setting->email }} </address>
                                                </div>
                                            </td>
                                            <td class="p-0 m-0  text-end border-bottom">
                                                <h3>#{{ sprintf("%s/%s/%04d", 'MLM', $order->financial_year, $order->invoice_no) }}</h3>
                                                <h5>Date Issued: {{ $order->created_at }}</h5>
                                                <!-- <h5>Due Date: 12-07-2021</h5> -->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="pt-5">
                                <table class="table mb-0 text-nowrap p-0 invoice-header" style="color: #282f53; border-bottom-width: 0;">
                                    <tbody>
                                        <tr class="">
                                            <td class="p-0 m-0">
                                                <p class="h3">Invoice To:</p>
                                                @if($order->address_id)
                                                <p class="fs-18 fw-semibold mb-0">{{ $order->address->name }}</p>
                                                <address>
                                                    {{ $order->address->address }}
                                                    <br>{{ $order->address->city }}, {{ $order->address->state }}
                                                    <br>{{ $order->address->country }}, {{ $order->address->pincode }}
                                                    <br>{{ $order->address->email }}
                                                    <br>{{ $order->address->mobile }}
                                                </address>
                                                @else
                                                <p class="fs-18 fw-semibold mb-0">{{ $order->user->name }}</p>
                                                <address>
                                                    {{ $order->user->email }}
                                                    <br>{{ $order->user->mobile }}
                                                </address>
                                                @endif
                                            </td>
                                            <td class="p-0 m-0 text-end">
                                                <p class="h4 fw-semibold">Payment Details:</p>
                                                <p class="mb-1">Total Amount: ₹ {{ $order->total_amount }} /-</p>
                                                <p class="mb-1">Payment Status: {{ $order->payment_status }}</p>
                                                <p class="mb-1">Txn Id: {{ $order->txn_id }}</p>
                                                <p class="mb-1">Order Status: {{ $order->status }}</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <h4>Order Details : </h4>
                            <div class="table-responsive push mt-5 mb-5">
                                <table class="table table-bordered table-hover mb-0 text-nowrap">
                                    <tbody>
                                        <tr>
                                            <td style="width: 100px;">
                                                <div class="d-flex">
                                                    <span class="avatar bradius" style="background-image: url('{{ url('storage/kit',$order->kit->image)}}');"></span>
                                                </div>
                                            </td>
                                            <td style="width: 200px">
                                                <p class="font-w600 mb-1" style="text-transform: capitalize;">{{ $order->kit->name }}</p>
                                                <div class="text-muted">
                                                    <div class="text-muted" style="max-width: 300px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{{ $order->kit->description }}</div>
                                                </div>
                                            </td>
                                            <!-- <td class="text-center">2</td> -->
                                            <!-- <td class="text-end">$674</td> -->
                                            <td class="text-end">₹ {{ $order->total_amount }} /-</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive push">
                                <table class="table table-bordered table-hover mb-0 text-nowrap">
                                    <tbody>
                                        <tr class="">
                                            <th class="">Sr. No.</th>
                                            <th>Item</th>
                                            <!-- <th class="text-center">Quantity</th> -->
                                            <!-- <th class="text-end">Unit Price</th> -->
                                            <th class="text-end">Price</th>
                                        </tr>
                                        @foreach($order->products as $key => $product)
                                        <tr>
                                            <td class="">{{ $key + 1 }}</td>
                                            <td>
                                                <p class="font-w600 mb-1" style="text-transform: capitalize;">{{ $product->name }}</p>
                                                <!-- <div class="text-muted">
                                                    <div class="text-muted" style="max-width: 300px;  overflow: hidden; text-overflow: ellipsis;">{{ $product->product->excerpt }}</div>
                                                </div> -->
                                            </td>
                                            <!-- <td class="text-center">2</td> -->
                                            <!-- <td class="text-end">$674</td> -->
                                            <td class="text-end">₹ {{ $product->price }} /-</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="2" class="fw-bold text-uppercase text-end">Total</td>
                                            <td class="fw-bold text-end h4">₹ {{ $order->total_amount }} /-</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- <div class="card-footer text-end"> <button type="button" class="btn btn-primary mb-1" onclick="javascript:window.print();"><i class="si si-wallet"></i> Pay Invoice</button> <button type="button" class="btn btn-secondary mb-1" onclick="javascript:window.print();"><i class="si si-paper-plane"></i> Send Invoice</button> <button type="button" class="btn btn-danger mb-1" onclick="javascript:window.print();"><i class="si si-printer"></i> Print Invoice</button> </div> -->
                    </div>
                </div> <!-- COL-END -->
            </div> <!-- ROW-1 CLOSED -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>
@stop