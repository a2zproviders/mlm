@extends('frontend.layout.usermaster')
@section('title', 'Products')
@section('keywords', 'Products Keywords')
@section('description', 'Products Description')
@section('contant')
@section('style')
@stop
<div class="main-content mt-0 hor-content">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Products</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Products</li>
                    </ol>
                </div>
            </div> <!-- PAGE-HEADER END -->
            <!-- ROW-1 OPEN -->
            <div class="row row-cards">
                <div class="col-xl-12 col-lg-12">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card p-0">
                                <div class="card-body p-4">
                                    <div class="row">
                                        <div class="col-xl-5 col-lg-8 col-md-8 col-sm-8">
                                            <form method="GET">
                                                <div class="input-group d-flex w-100 float-start">
                                                    <input type="text" name="s" class="form-control border-end-0 my-2" placeholder="Search ...">
                                                    <button class="btn input-group-text bg-transparent border-start-0 text-muted my-2">
                                                        <i class="fe fe-search text-muted" aria-hidden="true"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- <div class="col-xl-7 col-lg-4 col-md-4 col-sm-4">
                                            <ul class="nav item2-gl-menu float-end my-2">
                                                <li class="border-end"><a href="#tab-11" class="show active" data-bs-toggle="tab" title="List style"><i class="fa fa-th"></i></a></li>
                                                <li><a href="#tab-12" data-bs-toggle="tab" class="" title="Grid"><i class="fa fa-list"></i></a></li>
                                            </ul>
                                        </div> -->
                                        <!-- <div class="col-xl-3 col-lg-12"> <a href="add-product.html" class="btn btn-primary btn-block float-end my-2"><i class="fa fa-plus-square me-2"></i>New Product</a> </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-11">
                            <div class="row">
                                @foreach($lists as $list)
                                <div class="col-md-4 col-xl-3">
                                    <div class="card">
                                        <div class="product-grid6">
                                            <div class="product-image6 p-5">
                                                <!-- <ul class="icons">
                                                    <li> <a href="shop-description.html" class="bg-primary text-white border-primary border"> <i class="fe fe-eye"> </i> </a> </li>
                                                    <li><a href="add-product.html" class="bg-success text-white border-success border"><i class="fe fe-edit"></i></a></li>
                                                    <li><a href="#" class="bg-danger text-white border-danger border"><i class="fe fe-x"></i></a></li>
                                                </ul>  -->
                                                <a href="{{ route('user.product.detail',$list->slug) }}" class="bg-light">

                                                    @foreach($list->images as $key => $img)
                                                    @if($key < 1) <img class="img-fluid br-7 w-100" src="{{ url('storage/product',$img->image) }}" alt="img">
                                                        @endif
                                                        @endforeach
                                                </a>
                                            </div>
                                            <div class="card-body pt-0">
                                                <div class="product-content text-center">
                                                    <h1 class="title fw-bold fs-20"><a href="{{ route('user.product.detail',$list->slug) }}">{{ $list->name }}</a></h1>
                                                    <!-- <div class="mb-2 text-warning"> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star-half-o text-warning"></i> <i class="fa fa-star-o text-warning"></i> </div> -->
                                                    <div class="price">₹{{ $list->price }} /-
                                                        <!-- <span class="ms-4">$799</span>  -->
                                                    </div>
                                                    <p class="">{{ $list->excerpt }}</p>
                                                </div>
                                            </div>
                                            <div class="card-footer text-center">
                                                <a href="{{ route('user.checkout',$list->slug) }}" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart me-2"></i>Buy Now</a>
                                                <!-- <a href="wishlist.html" class="btn btn-outline-primary mb-1"><i class="fe fe-heart me-2 wishlist-icon"></i>Add to wishlist</a> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div> <!-- COL-END -->
                </div> <!-- ROW-1 CLOSED -->
            </div> <!-- ROW-1 END -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>
@stop