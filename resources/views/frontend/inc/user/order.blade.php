@extends('frontend.layout.usermaster')
@section('title', 'Orders')
@section('keywords', 'Orders Keywords')
@section('description', 'Orders Description')
@section('contant')
@section('style')

<!-- Edit Table -->
<!-- <link href="{{ url('assets/plugins/edit-table/edit-table.css') }}" rel="stylesheet"> -->

<link href="{{ url('assets/plugins/datatable/css/buttons.bootstrap5.min.css') }}" rel="stylesheet">
<link href="{{ url('assets/plugins/datatable/responsive.bootstrap5.css') }}" rel="stylesheet" />

@stop
<div class="main-content hor-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Order List</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Order</li>
                    </ol>
                </div>
            </div> <!-- PAGE-HEADER END -->
            <!-- Row -->
            <div class="row row-sm">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Order List</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                @if($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    {{$message}}
                                </div>
                                @endif
                                @if($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    {{$message}}
                                </div>
                                @endif
                                <div id="responsive-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered text-nowrap border-bottom dataTable no-footer" id="responsive-datatable" role="grid" aria-describedby="responsive-datatable_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">S. No.</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Invoice No.</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Total Amount</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Payment Type</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Payment Status</th>
                                                        <th class="wd-15p border-bottom-0 sorting" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Txn Id</th>
                                                        <th class="wd-25p border-bottom-0 sorting" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Created at</th>
                                                        <th class="wd-15p border-bottom-0 sorting sorting_asc" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Status</th>
                                                        <!-- <th class="wd-25p border-bottom-0" tabindex="0" aria-controls="responsive-datatable" rowspan="1" colspan="1">Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($lists as $key => $list)
                                                    <tr>
                                                        @if(method_exists($lists, 'links'))
                                                        <td>{{ $key + $lists->firstItem() }}.</td>
                                                        @else
                                                        <td>{{ $key + 1 }}.</td>
                                                        @endif
                                                        <td><a href="{{ route('user.order.invoice',$list->id) }}" title="view details"><i class="fe fe-eye"> </i> {{ sprintf("%s/%s/%04d", 'MLM', $list->financial_year, $list->invoice_no) }}</a></td>
                                                        <td>₹ {{ $list->total_amount }} /-</td>
                                                        <td>{{ $list->payment_type }}</td>
                                                        <td>{{ $list->payment_status }}</td>
                                                        <td>{{ $list->txn_id }}</td>
                                                        <td>{{ $list->created_at }}</td>
                                                        <td>
                                                            <div class="mt-sm-1 d-block">
                                                                @if($list->status == 'pending')
                                                                <a href="#">
                                                                    <span class="badge bg-success-transparent rounded-pill text-success p-2 px-3">Pending</span>
                                                                </a>
                                                                @endif
                                                                @if($list->status == 'accepted')
                                                                <a href="#">
                                                                    <span class="badge bg-warning-transparent rounded-pill text-warning p-2 px-3">Accepted</span>
                                                                </a>
                                                                @endif
                                                                @if($list->status == 'shipped')
                                                                <a href="#">
                                                                    <span class="badge bg-warning-transparent rounded-pill text-warning p-2 px-3">Shipped</span>
                                                                </a>
                                                                @endif
                                                                @if($list->status == 'received')
                                                                <a href="#">
                                                                    <span class="badge bg-warning-transparent rounded-pill text-warning p-2 px-3">Received</span>
                                                                </a>
                                                                @endif
                                                                @if($list->status == 'canceled')
                                                                <a href="#">
                                                                    <span class="badge bg-warning-transparent rounded-pill text-danger p-2 px-3">Canceled</span>
                                                                </a>
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <!-- <td>
                                                            <div class="btn-list">
                                                                <a href="{{ route('admin.user.edit',$list->id) }}" class="btn btn-sm btn-primary">
                                                                    <span class="fe fe-edit"> </span>
                                                                </a>
                                                                <a href="#" data-url="{{ route('admin.user.delete',$list->id) }}" id="single_delete" class="btn btn-sm btn-danger">
                                                                    <span class="fe fe-trash-2"> </span>
                                                                </a>
                                                            </div>
                                                        </td> -->
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        @if(method_exists($lists, 'links'))
                                        @include('frontend.templete.pagination', ['paginator' => $lists,'link_limit'=>4])
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>
@section('script')

<script>
    function message() {
        swal({
            title: "Success",
            text: "Your Order is placed successfully.",
            type: "success"
        });
    }

    function removeParam(parameter) {
        var url = document.location.href;
        var urlparts = url.split('?');

        if (urlparts.length >= 2) {
            var urlBase = urlparts.shift();
            var queryString = urlparts.join("?");

            var prefix = encodeURIComponent(parameter) + '=';
            var pars = queryString.split(/[&;]/g);
            for (var i = pars.length; i-- > 0;)
                if (pars[i].lastIndexOf(prefix, 0) !== -1)
                    pars.splice(i, 1);
            url = urlBase + '?' + pars.join('&');
            window.history.pushState('', document.title, url); // added this line to push the new url directly to url bar .

        }
        return url;
    }
    var queryString = window.location.search;
    var parameters = new URLSearchParams(queryString);
    var value = parameters.get('msg');
    console.log('params', value);
    if (value) {
        setTimeout(message(), 100000)
        console.log(removeParam('msg'));
    }
</script>

<!-- Edit Table -->
<script src="{{ url('assets/plugins/edit-table/edit-table.js') }}"></script>
<script src="{{ url('assets/plugins/edit-table/bst-edittable.js') }}"></script>

<script src="{{ url('assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/dataTables.bootstrap5.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/buttons.bootstrap5.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/jszip.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/buttons.html5.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/buttons.print.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/buttons.colVis.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/responsive.bootstrap5.min.js') }}"></script>
<script src="{{ url('assets/js/table-data.js') }}"></script>

@stop
@stop