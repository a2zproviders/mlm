@extends('frontend.layout.usermaster')
@section('title', 'Profile')
@section('keywords', 'Profile Keywords')
@section('description', 'Profile Description')
@section('contant')
@section('style')
@stop
<div class="main-content mt-0 hor-content">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container">
            <!-- PAGE-HEADER -->
            <div class="mt-3">
                @if($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    {{$message}}
                </div>
                @endif
                @if($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    {{$message}}
                </div>
                @endif
                @if(count($errors->all()))
                @foreach($errors->all() as $error)
                <div class="alert alert-danger">
                    {{$error}}
                </div>
                @endforeach
                @endif
            </div>
            <div class="page-header">
                <h1 class="page-title">Edit Profile</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Profile</li>
                    </ol>
                </div>
            </div> <!-- PAGE-HEADER END -->
            <!-- ROW-1 OPEN -->
            <div class="row">
                <div class="col-xl-4">
                    <div class="card">
                        {{ Form::open(['url' => route('user.changepassword'), 'method'=>'POST', 'files' => true, 'class' => 'needs-validation','novalidate']) }}
                        <div class="card-header">
                            <div class="card-title">Edit Password</div>
                        </div>
                        <div class="card-body">
                            <div class="text-center chat-image mb-5">
                                <div class="avatar avatar-xxl chat-profile mb-3 brround"> <a class="" href="javascript:void(0)"><img alt="avatar" src="{{ url('storage/user',$user->image) }}" class="brround"></a> </div>
                                <div class="main-chat-msg-name"> <a href="javascript:void(0)">
                                        <h5 class="mb-1 text-dark fw-semibold" style="text-transform: capitalize;">{{ $user->name }}</h5>
                                    </a>
                                    <p class="text-muted mt-0 mb-0 pt-0 fs-13">Lavel {{ $user->lavel }}</p>
                                </div>
                            </div>    
                            {{ csrf_field() }}
                            <div class="form-group"> <label class="form-label">Current Password</label>
                                <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                                    <a href="#" class="input-group-text bg-white text-muted"> <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i> </a>
                                    <input class="input100 form-control" name="oldpassword" type="password" placeholder="Current Password" required>
                                    <div class="invalid-feedback">Please enter current password.</div>
                                </div>
                            </div>
                            <div class="form-group"> <label class="form-label">New Password</label>
                                <div class="wrap-input100 validate-input input-group" id="Password-toggle1">
                                    <a href="#" class="input-group-text bg-white text-muted"> <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i> </a>
                                    <input class="input100 form-control" type="password" name="newpassword" placeholder="New Password" required>
                                    <div class="invalid-feedback">Please enter new password.</div>
                                </div>
                            </div>
                            <div class="form-group"> <label class="form-label">Confirm Password</label>
                                <div class="wrap-input100 validate-input input-group" id="Password-toggle2">
                                    <a href="#" class="input-group-text bg-white text-muted"> <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i> </a>
                                    <input class="input100 form-control" type="password" name="confirmpassword" placeholder="Confirm Password" required>
                                    <div class="invalid-feedback">Please enter confirm password.</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-end">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                        </form>
                    </div>
                    <div class="card panel-theme">
                        <div class="card-header">
                            <div class="float-start">
                                <h3 class="card-title">Details</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="card-body no-padding">
                            <ul class="list-group no-margin">
                                <li class="list-group-item d-flex ps-3">
                                    <div class="social social-profile-buttons me-2"> <a class="social-icon text-primary" href="mailto:{{ $user->email }}"><i class="fe fe-mail"></i></a> </div> <span class="my-auto">{{ $user->email }}</span>
                                </li>
                                <!-- <li class="list-group-item d-flex ps-3">
                                    <div class="social social-profile-buttons me-2"> <a class="social-icon text-primary" href=""><i class="fe fe-globe"></i></a> </div> <span class="my-auto">www.abcd.com</span>
                                </li> -->
                                <li class="list-group-item d-flex ps-3">
                                    <div class="social social-profile-buttons me-2"> <a class="social-icon text-primary" href="mailto:{{ $user->mobile }}"><i class="fe fe-phone"></i></a> </div> <span class="my-auto">{{ $user->mobile }}</span>
                                </li>
                                <li class="list-group-item d-flex ps-3">
                                    <div class="social social-profile-buttons me-2"> <a class="social-icon text-primary" href="javascript:void(0)"><i class="fe fe-arrow-right"></i></a> </div> <span class="my-auto">Lavel {{ $user->lavel }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="card">
                        {{ Form::open(['url' => route('user.editprofile'), 'method'=>'POST', 'files' => true, 'class' => 'needs-validation','novalidate']) }}
                        <div class="card-header">
                            <h3 class="card-title">Edit Profile</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Name','id'=>'name','required'=>'required'])}}
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        {{Form::email('email', '', ['class' => 'form-control', 'placeholder'=>'Email','id'=>'email','required'=>'required','disabled'])}}
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <label for="mobile">Mobile</label>
                                        {{Form::number('mobile', '', ['class' => 'form-control', 'placeholder'=>'Mobile','id'=>'mobile','required'=>'required','disabled'])}}
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">About Me</label>
                                        <textarea class="form-control" name="bio" rows="6">{{ $user->bio }}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <label for="aadhar">Aadhar Number</label>
                                        {{Form::number('aadhar_number', '', ['class' => 'form-control', 'placeholder'=>'Aadhar Number','id'=>'aadhar','required'=>'required'])}}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <label for="pan">Pan Number</label>
                                        {{Form::text('pan_number', '', ['class' => 'form-control', 'placeholder'=>'Pan Number','id'=>'pan','required'=>'required'])}}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control" name="image" id="image" placeholder="Iamge">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <label for="aadhar_front">Aadharcard Front</label>
                                        <input type="file" class="form-control" name="aadhar_front" id="aadhar_front" placeholder="Aadharcard Front">
                                        @if($user->aadhar_front)
                                        <div><img src="{{ url('storage/aadharcard',$user->aadhar_front) }}" width="100%" class="mt-3"></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <label for="aadhar_back">Aadharcard Back</label>
                                        <input type="file" class="form-control" name="aadhar_back" id="aadhar_back" placeholder="Aadharcard Back">
                                        @if($user->aadhar_back)
                                        <div><img src="{{ url('storage/aadharcard',$user->aadhar_back) }}" width="100%" class="mt-3"></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <label for="pan_image">Pancard Image</label>
                                        <input type="file" class="form-control" name="pan_image" id="pan_image" placeholder="Pancard Image">
                                        @if($user->pan_image)
                                        <div><img src="{{ url('storage/pancard',$user->pan_image) }}" width="100%" class="mt-3"></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-end">
                            <!-- <a href="#" class="btn btn-success my-1">Save</a>  -->

                            <button type="submit" class="btn btn-success my-1">Update</button>
                            <!-- <a href="#" class="btn btn-danger my-1">Cancel</a>  -->
                        </div>
                        </form>
                    </div>
                </div>
            </div> <!-- ROW-1 CLOSED -->
        </div>
        <!--CONTAINER CLOSED -->
    </div>
</div>
@stop