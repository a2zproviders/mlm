@extends('frontend.layout.master')
@section('title','Home Page')
@section('keywords','Home Page Keywords')
@section('description','Home Page Description')
@section('contant')
@section('style')
<!-- sdgjdfogjifd -->
<style>
    @media (max-width: 576px) {
        .carousel-height {
            /* height: 400px; */
        }
    }

    @media (min-width: 768px) {
        .carousel-height {
            height: 300px;
        }
    }

    @media (min-width: 992px) {
        .carousel-height {
            height: 400px;
        }
    }

    @media (max-width: 990px) and (min-width: 767px) {
        .carousel-caption {
            top: 35%;
        }
    }
</style>
@stop
<div class="main-content hor-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container" id="home">
            <!-- SLIDER OPEN -->
            <div id="carousel-captions" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-height carousel-item active"> <img class="d-block w-100 br-5" alt="" src="{{ url('assets/images/media/9.jpg') }}" data-bs-holder-rendered="true">
                        <div class="carousel-item-background d-none d-md-block"></div>
                        <div class="carousel-caption d-none d-md-block">
                            <h3>Slide label</h3>
                            <p>Secure other greater pleasures</p>
                        </div>
                    </div>
                    <div class="carousel-height carousel-item"> <img class="d-block w-100 br-5" alt="" src="{{ url('assets/images/media/10.jpg') }}" data-bs-holder-rendered="true">
                        <div class="carousel-item-background d-none d-md-block"></div>
                        <div class="carousel-caption d-none d-md-block">
                            <h3>Slide label</h3>
                            <p>Secure other greater pleasures</p>
                        </div>
                    </div>
                    <div class="carousel-height carousel-item"> <img class="d-block w-100 br-5" alt="" src="{{ url('assets/images/media/11.jpg') }}" data-bs-holder-rendered="true">
                        <div class="carousel-item-background d-none d-md-block"></div>
                        <div class="carousel-caption d-none d-md-block">
                            <h3>Slide label</h3>
                            <p>Secure other greater pleasures</p>
                        </div>
                    </div>
                    <div class="carousel-height carousel-item"> <img class="d-block w-100 br-5" alt="" src="{{ url('assets/images/media/12.jpg') }}" data-bs-holder-rendered="true">
                        <div class="carousel-item-background d-none d-md-block"></div>
                        <div class="carousel-caption d-none d-md-block">
                            <h3>Slide label</h3>
                            <p>Secure other greater pleasures</p>
                        </div>
                    </div>
                    <div class="carousel-height carousel-item"> <img class="d-block w-100 br-5" alt="" src="{{ url('assets/images/media/13.jpg') }}" data-bs-holder-rendered="true">
                        <div class="carousel-item-background d-none d-md-block"></div>
                        <div class="carousel-caption d-none d-md-block">
                            <h3>Slide label</h3>
                            <p>Secure other greater pleasures</p>
                        </div>
                    </div>
                </div> <a class="carousel-control-prev" href="#carousel-captions" role="button" data-bs-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carousel-captions" role="button" data-bs-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
            </div> <!-- SLIDER CLOSE -->
        </div>
        <div class="main-container container">
            <div class="">


                <!-- ABOUT 1 OPEN-->

                <div class="row" id="about-us" style="margin-top: 50px;">
                    <div class="col-lg-12">
                        <div class="row py-5">
                            <div class="text-center">
                                <h4 class="display-5 fw-semibold">Our mission is to make Work Meaningful.</h4>
                                <p class="">If you are going to use a passage of Lorem Ipsum, you need to as necessary All the Lorem Ipsum generators on the Internet tend to repeat Various versions have evolved over repeat Various versions have evolved over.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-lg-12"> <img src="{{ url('assets/images/media/team.jpg') }}" alt="" width="100%" class="br-5"> </div> -->
                </div> <!-- ABOUT 1 CLOSED -->

                <!-- ABOUT 2 OPEN-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card mt-7">
                            <div class="card-body p-5 text-dark">
                                <div class="statistics-info p-4">
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="">
                                                <p>{!! $about_us->description !!}</p>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="text-center"> <img src="{{ url('storage/page',$about_us->image) }}" alt=""> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- ABOUT 2 CLOSE-->

                <!-- PRODUCT OPEN-->
                <div class="row" style="margin-top: 50px;">
                    <h2 class="text-center fw-semibold" style="margin-bottom: 50px;">Kits</h2>
                    @foreach($kits as $kit)
                    <div class="col-md-4 col-xl-3">
                        <div class="card">
                            <div class="product-grid6">
                                <div class="product-image6 p-5">
                                    <a href="{{ route('kit.detail',$kit->slug) }}" class="bg-light"> <img class="img-fluid br-7 w-100 product-height" src="{{ url('storage/kit',$kit->image) }}" alt="img"> </a>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="product-content text-center">
                                        <h1 class="title fw-bold fs-20"><a href="{{ route('kit.detail',$kit->slug) }}">{{ $kit->name }}</a></h1>
                                        <!-- <div class="mb-2 text-warning"> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star-half-o text-warning"></i> <i class="fa fa-star-o text-warning"></i> </div> -->
                                        <div class="price">₹{{ $kit->price }} /- </div>
                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    @if(auth()->guard('user')->user())
                                    <a href="{{ route('user.checkout',$kit->slug) }}" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart me-2"></i>Buy Now</a>
                                    @else
                                    <a href="{{ route('auth.register',$kit->slug) }}" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart me-2"></i>Buy For Registration</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div><!-- PRODUCT CLOSE-->


                <!-- BLOG OPEN -->
                <div class="row" style="margin-top: 50px;">
                    <h2 class="text-center fw-semibold" style="margin-bottom: 50px;">Blogs</h2>
                    <!-- <h5>Our Leadership</h5> -->
                    @foreach($blogs as $blog)
                    <div class="col-sm-6 col-md-12 col-lg-6 col-xl-3">
                        <div class="card"> <a href="{{ route('blog.detail',$blog->slug) }}"><img class="card-img-top br-tr-7 br-tl-7" style="height: 250px;" src="{{ url('storage/blog',$blog->image) }}" alt="{{ $blog->name }}"></a>
                            <div class="card-body d-flex flex-column">
                                <h3><a href="{{ route('blog.detail',$blog->slug) }}"> {{ $blog->name }} </a></h3>
                                <div class="text-muted three-line-ellipsis"> {{ $blog->excerpt }} </div>
                                <!-- <div class="d-flex align-items-center pt-5 mt-auto">
                                    <div class="avatar brround avatar-md me-3 cover-image" data-bs-image-src="{{ url('assets/images/users/18.jpg') }}" style="background: url('{{ url('assets/images/users/18.jpg') }}') center center;"></div>
                                    <div> <a href="#" class="text-default">Megan Peters</a> <small class="d-block text-muted">1 days ago</small> </div>
                                    <div class="ms-auto"> <a href="javascript:void(0)" class="icon d-none d-md-inline-block text-muted"><i class="fe fe-heart me-1 border brround"></i></a> <a href="javascript:void(0)" class="icon d-none d-md-inline-block text-muted"><i class="fa fa-thumbs-o-up border brround"></i></a> </div>
                                </div> -->
                            </div>
                        </div>
                    </div> <!-- COL-END -->
                    @endforeach
                </div><!-- BLOG CLOSE -->

                <!-- ABOUT 3 OPEN-->
                <!-- <div class="row">
                    <div class="col-xl-6">
                        <div class="card mt-5">
                            <div class="card-body about-con pabout">
                                <h1 class="mb-4 fw-semibold">Why Sash ?</h1>
                                <h4 class="leading-normal">majority have suffered alteration in some form, by injected humour</h4>
                                <p class="leading-normal">There are many variations of passages of Lorem Ipsum available, but the majority have suffered by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to as necessary All the Lorem Ipsum generators on the Internet tend to repeat Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p><a href="#" class="btn btn-primary  mt-2">View More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 mt-5">
                        <div class="card">
                            <div class="card-body pabout">
                                <h1 class="mb-4 fw-semibold">What is Our Services?</h1>
                                <h4 class="leading-normal">majority have suffered alteration in some form, by injected humour</h4>
                                <p class="leading-normal">There are many variations of passages of Lorem Ipsum available, but the majority have suffered by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to as necessary All the Lorem Ipsum generators on the Internet tend to repeat Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p><a href="#" class="btn btn-primary mt-2">View More</a>
                            </div>
                        </div>
                    </div>
                </div>  -->
                <!-- ABOUT 3 CLOSE-->

                <!-- TEAM OPEN-->
                <div class="row" style="margin-top: 50px;">
                    <div class="text-center">
                        <h2 class="fw-semibold mb-2">Meet our creative minds</h2>
                        <h5>Our Leadership</h5>
                    </div> <!-- COL-OPEN -->
                    <div class="col-xl-3 col-lg-6 mt-3">
                        <div class="card">
                            <div class="card-body text-center"> <span class="avatar avatar-xxl bradius cover-image" data-bs-image-src="{{ url('assets/images/users/2.jpg') }}" style="background: url('{{ url('assets/images/users/2.jpg') }}') center center;"></span>
                                <h4 class="h4 mb-0 mt-3">Mike Rowe-Soft</h4>
                                <p class="card-text">Web designer</p>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
                                <div class="row user-social-detail text-center">
                                    <div class="social-profile me-4 rounded text-center bg-secondary-transparent"> <a href=""><i class="fa fa-google text-secondary"></i></a> </div>
                                    <div class="social-profile me-4 rounded text-center bg-danger-transparent"> <a href=""><i class="fa fa-facebook text-danger"></i></a> </div>
                                    <div class="social-profile rounded text-center bg-success-transparent"> <a href=""><i class="fa fa-twitter text-success"></i></a> </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- COL-END -->
                    <!-- COL-OPEN -->
                    <div class="col-xl-3 col-lg-6 mt-3">
                        <div class="card">
                            <div class="card-body text-center"> <span class="avatar avatar-xxl bradius cover-image" data-bs-image-src="{{ url('assets/images/users/2.jpg') }}" style="background: url('{{ url('assets/images/users/2.jpg') }}') center center;"></span>
                                <h4 class="h4 mb-0 mt-3">Mike Rowe-Soft</h4>
                                <p class="card-text">Web designer</p>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
                                <div class="row user-social-detail text-center">
                                    <div class="social-profile me-4 rounded text-center bg-secondary-transparent"> <a href=""><i class="fa fa-google text-secondary"></i></a> </div>
                                    <div class="social-profile me-4 rounded text-center bg-danger-transparent"> <a href=""><i class="fa fa-facebook text-danger"></i></a> </div>
                                    <div class="social-profile rounded text-center bg-success-transparent"> <a href=""><i class="fa fa-twitter text-success"></i></a> </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- COL-END -->
                    <!-- COL-OPEN -->
                    <div class="col-xl-3 col-lg-6 mt-3">
                        <div class="card">
                            <div class="card-body text-center"> <span class="avatar avatar-xxl bradius cover-image" data-bs-image-src="{{ url('assets/images/users/9.jpg') }}" style="background: url('{{ url('assets/images/users/9.jpg') }}') center center;"></span>
                                <h4 class="h4 mb-0 mt-3">Mike Rowe-Soft</h4>
                                <p class="card-text">Web designer</p>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
                                <div class="row user-social-detail text-center">
                                    <div class="social-profile me-4 rounded text-center bg-secondary-transparent"> <a href=""><i class="fa fa-google text-secondary"></i></a> </div>
                                    <div class="social-profile me-4 rounded text-center bg-danger-transparent"> <a href=""><i class="fa fa-facebook text-danger"></i></a> </div>
                                    <div class="social-profile rounded text-center bg-success-transparent"> <a href=""><i class="fa fa-twitter text-success"></i></a> </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- COL-END -->
                    <!-- COL-OPEN -->
                    <div class="col-xl-3 col-lg-6 mt-3">
                        <div class="card">
                            <div class="card-body text-center"> <span class="avatar avatar-xxl bradius cover-image" data-bs-image-src="{{ url('assets/images/users/4.jpg') }}" style="background: url('{{ url('assets/images/users/4.jpg') }}') center center;"></span>
                                <h4 class="h4 mb-0 mt-3">Mike Rowe-Soft</h4>
                                <p class="card-text">Web designer</p>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
                                <div class="row user-social-detail text-center">
                                    <div class="social-profile me-4 rounded text-center bg-secondary-transparent"> <a href=""><i class="fa fa-google text-secondary"></i></a> </div>
                                    <div class="social-profile me-4 rounded text-center bg-danger-transparent"> <a href=""><i class="fa fa-facebook text-danger"></i></a> </div>
                                    <div class="social-profile rounded text-center bg-success-transparent"> <a href=""><i class="fa fa-twitter text-success"></i></a> </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- COL-END -->
                </div> <!-- TEAM CLOSED -->

                <!-- SERVICE OPEN-->
                <div class="row" style="margin-top: 50px;">
                    <h2 class="text-center fw-semibold" style="margin-bottom: 50px;">Our Services</h2>
                    <div class="col-lg-12">
                        <div class="mt-3">
                            <div class="text-dark">
                                <div class="services-statistics">
                                    <div class="row text-center">
                                        <div class="col-xl-3 col-lg-6">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="counter-status">
                                                        <div class="counter-icon bg-primary-gradient box-shadow-primary"> <i class="icon icon-people text-white"></i> </div>
                                                        <h4 class="mb-2 fw-semibold">Branding</h4>
                                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-lg-6">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="counter-status">
                                                        <div class="counter-icon bg-secondary-gradient box-shadow-secondary"> <i class="icon icon-rocket text-white"></i> </div>
                                                        <h4 class="mb-2 fw-semibold">Development</h4>
                                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-lg-6">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="counter-statuss">
                                                        <div class="counter-icon bg-success-gradient box-shadow-success"> <i class="fe fe-instagram text-white"></i> </div>
                                                        <h4 class="mb-2 fw-semibold">Social Media</h4>
                                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-lg-6">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="counter-status">
                                                        <div class="counter-icon bg-danger-gradient box-shadow-danger"> <i class="fe fe-shopping-cart text-white"></i> </div>
                                                        <h4 class="mb-2 fw-semibold">E-Commerce</h4>
                                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- COL-END -->
                </div> <!-- SERVICE CLOSED -->

                <!-- FAQS AND CONTACT US OPEN-->
                @if($faqs->count())
                <div class="row5" id="faqs" style="margin-top: 50px;">
                    <h2 class="text-center fw-semibold" style="margin-bottom: 50px;">Faqs</h2>
                    <div class="col-md-12">
                        <div class="card">
                            <!-- <div class="card-header">
                                <h3 class="card-title">Faqs</h3>
                            </div> -->
                            <div class="card-body">
                                <div class="accordion" id="accordionExample">
                                    @foreach($faqs as $key => $faq)
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="heading{{ $faq->id }}">
                                            <button class="accordion-button @if($key > 1) collapsed @endif" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $faq->id }}" aria-expanded="{{ $key < 1 ? true : false }}" aria-controls="collapse{{ $faq->id }}"> {{ $faq->name }} </button>
                                        </h2>
                                        <div id="collapse{{ $faq->id }}" class="accordion-collapse collapse @if($key < 1) show @endif" aria-labelledby="heading{{ $faq->id }}" data-bs-parent="#accordionExample">
                                            <div class="accordion-body"> {{ $faq->description }} </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <h2 class="text-center fw-semibold" style="margin-bottom: 50px;" id="contact-us" style="margin-top: 50px;">Contact Us</h2>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Ask Question</h3>
                            </div>
                            <div class="card-body">
                                <h5>If Your Query Not Clarifified Post Your Question, My Customer Support will help You</h5>
                                <div class="pt-4">
                                    <div class="form-group"> <input type="text" class="form-control" id="name1" placeholder="Your Name"> </div>
                                    <div class="form-group"> <input type="email" class="form-control" id="email" placeholder="Email Address"> </div>
                                    <div class="form-group"> <textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Post Your Quesry"></textarea> </div> <a href="#" class="btn btn-primary">Send Question</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <!-- FAQS AND CONTACT US CLOSE-->

            </div>
        </div> <!-- CONTAINER CLOSED-->
    </div>
</div>
@stop