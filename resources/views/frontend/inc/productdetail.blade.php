@extends('frontend.layout.master')
@section('title', $product->name)
@section('keywords', $product->name)
@section('description', $product->name)
@section('contant')
<div class="main-content hor-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title" style="text-transform: capitalize;">{{ $product->name }}</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <!-- <li class="breadcrumb-item"><a href="{{ route('product') }}">Product</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page" style="text-transform: capitalize;">{{ $product->name }}</li>
                    </ol>
                </div>
            </div> <!-- PAGE-HEADER END -->
            <!-- ROW-1 OPEN -->
            <div class="row mt-5">
                <div class="col-xl-9 col-lg-8">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row row-sm">
                                        <div class="col-xl-5 col-lg-12 col-md-12">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="product-carousel">
                                                        <div id="Slider" class="carousel slide border" data-bs-ride="false">
                                                            <div class="carousel-inner">
                                                                @foreach($product->images as $key => $img)
                                                                <div class="carousel-item  @if($key < 1) active @endif p-0">
                                                                    <img src="{{ url('storage/product',$img->image)}}" alt="img" class="img-fluid mx-auto d-block">
                                                                    <!-- <div class="text-center mt-5 mb-5 btn-list"> </div> -->
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix carousel-slider">
                                                        <div id="thumbcarousel" class="carousel slide" data-bs-interval="t">
                                                            <div class="carousel-inner">
                                                                <ul class="carousel-item active">
                                                                    @foreach($product->images as $key => $img)
                                                                    <li data-bs-target="#Slider" data-bs-slide-to="{{ $key }}" class="thumb @if($key < 1) active @endif m-2"><img src="{{ url('storage/product',$img->image)}}" alt="img"></li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="details col-xl-7 col-lg-12 col-md-12 mt-4 mt-xl-0">
                                            <div class="mt-2 mb-4">
                                                <h3 class="mb-3" style="text-transform: capitalize;">{{ $product->name }}</h3>
                                                <!-- <p class="text-muted float-start me-3"> <span class="fa fa-star text-warning"></span> <span class="fa fa-star text-warning"></span> <span class="fa fa-star text-warning"></span> <span class="fa fa-star-half-o text-warning"></span> <span class="fa fa-star-o text-warning"></span> </p>
                                        <p class="text-muted mb-4">( 40 Customers Reviews) </p> -->
                                                <h4 class="mt-4"><b> </b></h4>
                                                <p>{{ $product->excerpt }}</p>
                                                <h3 class="mb-4">
                                                    <span class="me-2 fw-bold fs-25">₹ {{ $product->price }} /- </span>
                                                    <!-- <span><del class="fs-18 text-muted">$599</del></span> -->
                                                </h3>
                                                <hr>
                                                <!-- <div class="btn-list mt-4">
                                                    <a href="{{ route('user.checkout',$product->slug) }}" class="btn ripple btn-primary me-2"><i class="fe fe-shopping-cart"> </i> Buy Now</a>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <div class="card productdesc">
                                <div class="card-body">
                                    <div class="panel panel-primary">
                                        <div class="panel-body tabs-menu-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab5">
                                                    <h4 class="mb-5 mt-3 fw-bold">Description :</h4>
                                                    <p class="mb-3 fs-15">
                                                        {{ $product->description }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- ROW-1 CLOSED -->
                </div>
                <div class="col-xl-3 col-lg-4">
                    <div class="col-md-12 col-lg-12">
                        @if($popular_kits->count())
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Top Kits</div>
                            </div>
                            <div class="card-body">
                                <div class="">
                                    @foreach($popular_kits as $key => $kit)
                                    <div class="d-flex overflow-visible @if($key>1) mt-5 @endif"> <img class="avatar bradius avatar-xl me-4 p-2 bg-white border" src="{{ url('assets/images/pngs/4.jpg') }}" alt="avatar-img">
                                        <div class="media-body valign-middle oneline-ellipsis"> <a href="{{ route('kit.detail',$kit->slug) }}" class="fw-semibold text-dark" style="text-transform: capitalize;">{{ $kit->name }}</a>
                                            <p class="p-0 m-0 oneline-ellipsis">{{ $kit->description }}</p>
                                            <!-- <div class="mb-1 text-warning"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i> </div> -->
                                            <h5 class="fw-bold">₹{{ $kit->price }} /-</h5>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>
@stop