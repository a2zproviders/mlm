@section('style')
<style>
    .validate-input .input100 {
        width: 100%;
    }
</style>
@stop
<!-- PAGE -->
<div class="page">
    <div class="">
        <!-- Theme-Layout -->
        <!-- CONTAINER OPEN -->
        <div class="col col-login mx-auto mt-7">
            <div class="text-center"> <img src="{{ url('assets/images/brand/logo-white.png') }}" class="header-brand-img" alt=""> </div>
        </div>
        <div class="container-login100">
            <div class="wrap-login100 p-6">
                <!-- <form class="login100-form validate-form login100-form-register" action="javascript:void(0);" id="register" data-url="{{ route('ajex.check') }}"> -->
                <form class="login100-form validate-form login100-form-register registerForm" method="POST" files="true" enctype="multipart/form-data" id="registerForm" data-url="{{ route('auth.kyc.store',[$id,$kit->slug]) }}">
                    <span class="login100-form-title"> KYC Details </span>
                    <div id="errors" class="mb-3">
                    </div>
                    {{ csrf_field() }}

                    @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        {{$message}}
                    </div>
                    @endif
                    @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        {{$message}}
                    </div>
                    @endif
                    @if(count($errors->all()))
                    @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{$error}}
                    </div>
                    @endforeach
                    @endif
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for=""> Aadhar Number </label>
                            <div class="wrap-input100 validate-input">
                                <!-- <a href="#" class="input-group-text bg-white text-muted"> <i class="mdi mdi-account" aria-hidden="true"></i> </a> -->
                                <!-- <input class="input100 form-control" type="text" name="aadhar_number" placeholder="Aadhar Number" required minlength="12" maxlength="12"> -->
                                {{Form::text('aadhar_number', '', ['class' => 'input100 form-control','id'=>'aadhar_number', 'placeholder'=>'Aadhar Number','minlength'=>'12','maxlength'=>'12','required'=>'required'])}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for=""> Pan Number </label>
                            <div class="wrap-input100 validate-input">
                                <!-- <a href="#" class="input-group-text bg-white text-muted"> <i class="zmdi zmdi-email" aria-hidden="true"></i> </a> -->
                                <!-- <input class="input100 form-control" type="text" name="pan_number" placeholder="Pan Number" required minlength="10" maxlength="10"> -->
                                {{Form::text('pan_number', '', ['class' => 'input100 form-control','id'=>'pan_number', 'placeholder'=>'Pan Number','minlength'=>'10','maxlength'=>'10','required'=>'required'])}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="aadhar_front"> Aadhar Front </label>
                            <div class="wrap-input100 validate-input">
                                <input type="file" class="form-control" id="aadhar_front" name="aadhar_front" required id="image" placeholder="Image">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="aadhar_back"> Aadhar Back </label>
                            <div class="wrap-input100 validate-input">
                                <input type="file" class="form-control" id="aadhar_back" name="aadhar_back" required id="image" placeholder="Image">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="pan_image"> Pancard Image </label>
                            <div class="wrap-input100 validate-input">
                                <input type="file" class="form-control" id="pan_image" name="pan_image" required id="image" placeholder="Image">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="image"> Passport Size Photo </label>
                            <div class="wrap-input100 validate-input">
                                <input type="file" class="form-control" id="image" name="image" required id="image" placeholder="Image">
                            </div>
                        </div>
                        <input type="hidden" name="txn_id" id="paymentID">

                    </div>
                    <div class="container-login100-form-btn">
                        <button type="submit" class="login100-form-btn btn-primary"> Submit </button>
                    </div>
                    <div class="text-center pt-3">
                        <p class="text-dark mb-0">Already have account?<a href="{{ route('auth.login') }}" class="text-primary ms-1">Sign In</a></p>
                    </div>
                    <div class="row mt-5">
                        <div class="col-md-4">
                            <img src="{{ url('storage/kit',$kit->image) }}" width="100%" />
                        </div>
                        <div class="col-md-8">
                            <h4>{{ $kit->name }}</h4>
                            <p class="three-line-ellipsis">{{ $kit->description }}</p>
                            <h5>₹{{ $kit->price }}/-</h5>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End PAGE -->

@section('script')

<script src="{{ url('https://checkout.razorpay.com/v1/checkout.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"> </script>

<script>
    $('#rzp-footer-form').submit(function(e) {
        var button = $(this).find('button');
        var parent = $(this);
        button.attr('disabled', 'true').html('Please Wait...');
        $.ajax({
            method: 'get',
            url: this.action,
            data: $(this).serialize(),
            complete: function(r) {
                console.log('complete');
                //console.log(r);
            }
        })
        return false;
    })

    function message() {
        swal({
            title: "Success",
            text: "Your Order is placed successfully.",
            type: "success"
        });
    }

    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        let form = $('#registerForm');
        let dataurl = $('#registerForm').data('url');

        $("#paymentDetail").removeAttr('style');
        if (transaction) {
            $('#paymentID').val(transaction.razorpay_payment_id);
        }

        form.attr('action', dataurl);
        form.attr('method', 'post');
        form.removeAttr('id');
        $('.registerForm').submit();

        // registerForm
        // $.ajax({
        //     method: 'post',
        //     url: dataurl,
        //     data: formdata,
        //     headers: {
        //         "Content-Type": "multipart/form-data",
        //     },
        //     dataType: "json",
        //     cache: false,
        //     processData: false,
        //     success: function(r) {
        //         console.log('sdffgj');
        //     }
        // })
    }

    var options = {
        key: "{{ env('RAZORPAY_KEY') }}",
        amount: "{{ $kit->price * 100 }}",
        name: 'MLM',
        description: '',
        image: '{{ url("assets/images/brand/logo-3.png") }}',
        // prefill: {
        //     "email": "",
        //     "contact": ""
        // },
        handler: demoSuccessHandler
    }

    window.r = new Razorpay(options);
    $(document).on('submit', '#registerForm', function(e) {
        is_valid = $(this).valid();
        console.log(is_valid);
        if (is_valid) {
            e.preventDefault();
            r.open();

        }
    });
</script>
@stop