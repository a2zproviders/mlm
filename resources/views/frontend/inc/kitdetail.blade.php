@extends('frontend.layout.master')
@section('title', $kit->name)
@section('keywords', $kit->name)
@section('description', $kit->name)
@section('contant')
<div class="main-content hor-content mt-0">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title" style="text-transform: capitalize;">{{ $kit->name }}</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('kit') }}">Kit</a></li>
                        <li class="breadcrumb-item active" aria-current="page" style="text-transform: capitalize;">{{ $kit->name }}</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- ROW-1 OPEN -->
            <div class="row mt-5">
                <div class="col-xl-9 col-lg-8">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row row-sm">
                                        <div class="col-xl-5 col-lg-12 col-md-12">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <img src="{{ url('storage/kit',$kit->image) }}" alt="img" class="img-fluid mx-auto d-block">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="details col-xl-7 col-lg-12 col-md-12 mt-4 mt-xl-0">
                                            <div class="mt-2 mb-4">
                                                <h3 class="mb-3" style="text-transform: capitalize;">{{ $kit->name }}</h3>
                                                <!-- <p class="text-muted float-start me-3"> <span class="fa fa-star text-warning"></span> <span class="fa fa-star text-warning"></span> <span class="fa fa-star text-warning"></span> <span class="fa fa-star-half-o text-warning"></span> <span class="fa fa-star-o text-warning"></span> </p>
                                        <p class="text-muted mb-4">( 40 Customers Reviews) </p> -->
                                                <h4 class="mt-4"><b></b></h4>
                                                <p>{{ $kit->description }}</p>
                                                <h3 class="mb-4">
                                                    <span class="me-2 fw-bold fs-25">₹{{ $kit->price }} /- </span>
                                                    <!-- <span><del class="fs-18 text-muted">$599</del></span> -->
                                                </h3>

                                                <hr>
                                                <div class="btn-list mt-4">

                                                    @if(auth()->guard('user')->user())
                                                    <a href="{{ route('user.checkout',$kit->slug) }}" class="btn ripple btn-primary me-2"><i class="fe fe-shopping-cart"> </i> Buy Now</a>
                                                    @else
                                                    <a href="{{ route('auth.register',$kit->slug) }}" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart me-2"></i>Buy Now</a>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <h4 class="fw-bold">Products</h4>
                            <div class="row">
                                @foreach($kit->products as $kit_product)
                                <div class="col-md-6 col-xl-4">
                                    <div class="card">
                                        <div class="product-grid6">
                                            <div class="product-image6 p-5">
                                                <a href="{{ route('product.detail',$kit_product->product->slug) }}" class="bg-light">
                                                    <div id="carousel-indicators4" class="carousel slide" data-bs-ride="carousel">
                                                        <div class="carousel-inner">
                                                            @foreach($kit_product->product->images as $key => $img)
                                                            <div class="carousel-item @if($key < 1) active @endif">
                                                                <img class="d-block w-100 br-5" alt="" src="{{ url('storage/product',$img->image) }}" data-bs-holder-rendered="true">
                                                            </div>
                                                            @endforeach
                                                            <!-- <div class="carousel-item active"> <img class="d-block w-100 br-5" alt="" src="{{ url('storage/kit',$kit->image) }}" data-bs-holder-rendered="true"> </div> -->
                                                        </div>
                                                    </div>
                                                    <!-- <img class="img-fluid br-7 w-100" src="{{ url('assets/images/pngs/4.jpg') }}" alt="img"> -->
                                                </a>
                                            </div>
                                            <div class="card-body pt-0">
                                                <div class="product-content text-center">
                                                    <h1 class="title fw-bold fs-20"><a href="{{ route('product.detail',$kit_product->product->slug) }}">{{ $kit_product->product->name }}</a></h1>
                                                    <!-- <div class="mb-2 text-warning"> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star text-warning"></i> <i class="fa fa-star-half-o text-warning"></i> <i class="fa fa-star-o text-warning"></i> </div> -->
                                                    <p class="p-0 m-0 three-line-ellipsis">{{ $kit_product->product->excerpt }}</p>
                                                    <div class="price">₹{{ $kit_product->product->price }} /-
                                                        <!-- <span class="ms-4">$799</span>  -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="card-footer text-center">
                                                <a href="#" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart me-2"></i>Buy Now</a>
                                                <a href="#" class="btn btn-outline-primary mb-1"><i class="fe fe-heart me-2 wishlist-icon"></i>Add to wishlist</a> 
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div> <!-- ROW-1 CLOSED -->
                </div>
                <div class="col-xl-3 col-lg-4">
                    <div class="col-md-12 col-lg-12">
                        @if($popular_kits->count())
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Top Kits</div>
                            </div>
                            <div class="card-body" style="min-height: 70vh;">
                                <div class="">
                                    @foreach($popular_kits as $key => $kit)
                                    <div class="d-flex overflow-visible @if($key>1) mt-5 @endif"> <img class="avatar bradius avatar-xl me-4 p-2 bg-white border" src="{{ url('assets/images/pngs/4.jpg') }}" alt="avatar-img">
                                        <div class="media-body valign-middle oneline-ellipsis"> <a href="{{ route('kit.detail',$kit->slug) }}" class="fw-semibold text-dark" style="text-transform: capitalize;">{{ $kit->name }}</a>
                                            <p class="p-0 m-0 oneline-ellipsis">{{ $kit->description }}</p>
                                            <!-- <div class="mb-1 text-warning"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i> </div> -->
                                            <h5 class="fw-bold">₹{{ $kit->price }} /-</h5>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div> <!-- CONTAINER CLOSED -->
    </div>
</div>
@stop