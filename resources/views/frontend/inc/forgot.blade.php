<!-- PAGE -->
<div class="page">
    <div class="">
        <!-- Theme-Layout -->
        <!-- CONTAINER OPEN -->
        <div class="col col-login mx-auto mt-7">
            <div class="text-center"> <img src="{{ url('assets/images/brand/logo-white.png') }}" class="header-brand-img" alt=""> </div>
        </div>
        <div class="container-login100">
            <div class="wrap-login100 p-6">
                <form class="login100-form validate-form"> <span class="login100-form-title pb-5"> Forgot Password </span>
                    <p class="text-muted">Enter the email address registered on your account</p>
                    <div class="wrap-input100 validate-input input-group" data-bs-validate="Valid email is required: ex@abc.xyz"> <a href="#" class="input-group-text bg-white text-muted"> <i class="zmdi zmdi-email" aria-hidden="true"></i> </a> <input class="input100 form-control" type="email" placeholder="Email"> </div>
                    <!-- <button type="button" class="submit"> <a type="button" class="btn btn-primary d-grid">Submit</a> </button> -->
                    <button class="login100-form-btn btn-primary" type="Submit"> Submit </button>
                    <div class="text-center mt-4">
                        <p class="text-dark mb-0">Forgot It?<a class="text-primary ms-1" href="{{ route('auth.login') }}">Send me Back</a></p>
                    </div>
                    <!-- <label class="login-social-icon"><span>OR</span></label>
                    <div class="d-flex justify-content-center"> <a href="#">
                            <div class="social-login me-4 text-center"> <i class="fa fa-google"></i> </div>
                        </a> <a href="#">
                            <div class="social-login me-4 text-center"> <i class="fa fa-facebook"></i> </div>
                        </a> <a href="#">
                            <div class="social-login text-center"> <i class="fa fa-twitter"></i> </div>
                        </a>
                    </div> -->
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End PAGE -->