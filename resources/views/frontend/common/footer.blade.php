</div>
<!-- Sidebar-right -->
<div class="sidebar sidebar-right sidebar-animate ps">
    <div class="panel panel-primary card mb-0 shadow-none border-0">
        <div class="tab-menu-heading border-0 d-flex p-3">
            <div class="card-title mb-0"><i class="fe fe-bell me-2"></i><span class=" pulse"></span>Notifications</div>
            <div class="card-options ms-auto"> <a href="javascript:void(0);" class="sidebar-icon text-end float-end me-3 mb-1" data-bs-toggle="sidebar-right" data-target=".sidebar-right"><i class="fe fe-x text-white"></i></a> </div>
        </div>
        <div class="panel-body tabs-menu-body latest-tasks p-0 border-0">
            <div class="tabs-menu border-bottom">
                <!-- Tabs -->
                <ul class="nav panel-tabs">
                    <li class=""><a href="#side1" class="active" data-bs-toggle="tab"><i class="fe fe-settings me-1"></i>Feeds</a></li>
                    <li><a href="#side2" data-bs-toggle="tab"><i class="fe fe-message-circle"></i> Chat</a></li>
                    <li><a href="#side3" data-bs-toggle="tab"><i class="fe fe-anchor me-1"></i>Timeline</a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="side1">
                    <div class="p-3 fw-semibold ps-5">Feeds</div>
                    <div class="card-body pt-2">
                        <div class="browser-stats">
                            <div class="row mb-4">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle brround bg-primary-transparent"><i class="fe fe-user text-primary"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">New user registered</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> <a href="#"><i class="fe fe-x"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle avatar-circle-secondary brround bg-secondary-transparent"><i class="fe fe-shopping-cart text-secondary"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">New order delivered</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> <a href="#"><i class="fe fe-x"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle avatar-circle-danger brround bg-danger-transparent"><i class="fe fe-bell text-danger"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">You have pending tasks</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> <a href="#"><i class="fe fe-x"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle avatar-circle-warning brround bg-warning-transparent"><i class="fe fe-gitlab text-warning"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">New version arrived</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> <a href="#"><i class="fe fe-x"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle avatar-circle-pink brround bg-pink-transparent"><i class="fe fe-database text-pink"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">Server #1 overloaded</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> <a href="#"><i class="fe fe-x"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle avatar-circle-info brround bg-info-transparent"><i class="fe fe-check-circle text-info"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">New project launched</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> <a href="#"><i class="fe fe-x"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="p-3 fw-semibold ps-5">Settings</div>
                    <div class="card-body pt-2">
                        <div class="browser-stats">
                            <div class="row mb-4">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle brround bg-primary-transparent"><i class="fe fe-settings text-primary"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">General Settings</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle avatar-circle-secondary brround bg-secondary-transparent"><i class="fe fe-map-pin text-secondary"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">Map Settings</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle avatar-circle-danger brround bg-danger-transparent"><i class="fe fe-headphones text-danger"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">Support Settings</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle avatar-circle-warning brround bg-warning-transparent"><i class="fe fe-credit-card text-warning"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">Payment Settings</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-sm-2 mb-sm-0 mb-3"> <span class="feeds avatar-circle avatar-circle-pink brround bg-pink-transparent"><i class="fe fe-bell text-pink"></i></span> </div>
                                <div class="col-sm-10 ps-sm-0">
                                    <div class="d-flex align-items-end justify-content-between ms-2">
                                        <h6 class="">Notification Settings</h6>
                                        <div> <a href="#"><i class="fe fe-settings me-1"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="side2">
                    <div class="list-group list-group-flush">
                        <div class="pt-3 fw-semibold ps-5">Today</div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/2.jpg" style="background: url(&quot;../assets/images/users/2.jpg&quot;) center center;"></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Addie Minstra</div>
                                    <p class="mb-0 fs-12 text-muted"> Hey! there I' am available.... </p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/11.jpg" style="background: url(&quot;../assets/images/users/11.jpg&quot;) center center;"><span class="avatar-status bg-success"></span></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Rose Bush</div>
                                    <p class="mb-0 fs-12 text-muted"> Okay...I will be waiting for you </p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/10.jpg" style="background: url(&quot;../assets/images/users/10.jpg&quot;) center center;"></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Claude Strophobia</div>
                                    <p class="mb-0 fs-12 text-muted"> Hi we can explain our new project...... </p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/13.jpg" style="background: url(&quot;../assets/images/users/13.jpg&quot;) center center;"></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Eileen Dover</div>
                                    <p class="mb-0 fs-12 text-muted"> New product Launching... </p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/12.jpg" style="background: url(&quot;../assets/images/users/12.jpg&quot;) center center;"><span class="avatar-status bg-success"></span></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Willie Findit</div>
                                    <p class="mb-0 fs-12 text-muted"> Okay...I will be waiting for you </p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/15.jpg" style="background: url(&quot;../assets/images/users/15.jpg&quot;) center center;"></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Manny Jah</div>
                                    <p class="mb-0 fs-12 text-muted"> Hi we can explain our new project...... </p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/4.jpg" style="background: url(&quot;../assets/images/users/4.jpg&quot;) center center;"></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Cherry Blossom</div>
                                    <p class="mb-0 fs-12 text-muted"> Hey! there I' am available....</p>
                                </a> </div>
                        </div>
                        <div class="pt-3 fw-semibold ps-5">Yesterday</div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/7.jpg" style="background: url(&quot;../assets/images/users/7.jpg&quot;) center center;"><span class="avatar-status bg-success"></span></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Simon Sais</div>
                                    <p class="mb-0 fs-12 text-muted">Schedule Realease...... </p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/9.jpg" style="background: url(&quot;../assets/images/users/9.jpg&quot;) center center;"></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Laura Biding</div>
                                    <p class="mb-0 fs-12 text-muted"> Hi we can explain our new project...... </p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/2.jpg" style="background: url(&quot;../assets/images/users/2.jpg&quot;) center center;"><span class="avatar-status bg-success"></span></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Addie Minstra</div>
                                    <p class="mb-0 fs-12 text-muted">Contact me for details....</p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/9.jpg" style="background: url(&quot;../assets/images/users/9.jpg&quot;) center center;"></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Ivan Notheridiya</div>
                                    <p class="mb-0 fs-12 text-muted"> Hi we can explain our new project...... </p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/14.jpg" style="background: url(&quot;../assets/images/users/14.jpg&quot;) center center;"></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Dulcie Veeta</div>
                                    <p class="mb-0 fs-12 text-muted"> Okay...I will be waiting for you </p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/11.jpg" style="background: url(&quot;../assets/images/users/11.jpg&quot;) center center;"></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Florinda Carasco</div>
                                    <p class="mb-0 fs-12 text-muted">New product Launching...</p>
                                </a> </div>
                        </div>
                        <div class="list-group-item d-flex align-items-center">
                            <div class="me-2"> <span class="avatar avatar-md brround cover-image" data-bs-image-src="../assets/images/users/4.jpg" style="background: url(&quot;../assets/images/users/4.jpg&quot;) center center;"><span class="avatar-status bg-success"></span></span> </div>
                            <div class=""> <a href="chat.html">
                                    <div class="fw-semibold text-dark" data-bs-toggle="modal" data-target="#chatmodel">Cherry Blossom</div>
                                    <p class="mb-0 fs-12 text-muted">cherryblossom@gmail.com</p>
                                </a> </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="side3">
                    <ul class="task-list timeline-task">
                        <li class="d-sm-flex mt-4">
                            <div> <i class="task-icon1"></i>
                                <h6 class="fw-semibold">Task Finished<span class="text-muted fs-11 ms-2 fw-normal">09 July 2021</span></h6>
                                <p class="text-muted fs-12">Adam Berry finished task on<a href="#" class="fw-semibold"> Project Management</a></p>
                            </div>
                            <div class="ms-auto d-md-flex me-3"> <a href="#" class="text-muted me-2"><span class="fe fe-edit"></span></a> <a href="#" class="text-muted"><span class="fe fe-trash-2"></span></a> </div>
                        </li>
                        <li class="d-sm-flex">
                            <div> <i class="task-icon1"></i>
                                <h6 class="fw-semibold">New Comment<span class="text-muted fs-11 ms-2 fw-normal">05 July 2021</span></h6>
                                <p class="text-muted fs-12">Victoria commented on Project <a href="#" class="fw-semibold"> AngularJS Template</a></p>
                            </div>
                            <div class="ms-auto d-md-flex me-3"> <a href="#" class="text-muted me-2"><span class="fe fe-edit"></span></a> <a href="#" class="text-muted"><span class="fe fe-trash-2"></span></a> </div>
                        </li>
                        <li class="d-sm-flex">
                            <div> <i class="task-icon1"></i>
                                <h6 class="fw-semibold">New Comment<span class="text-muted fs-11 ms-2 fw-normal">25 June 2021</span></h6>
                                <p class="text-muted fs-12">Victoria commented on Project <a href="#" class="fw-semibold"> AngularJS Template</a></p>
                            </div>
                            <div class="ms-auto d-md-flex me-3"> <a href="#" class="text-muted me-2"><span class="fe fe-edit"></span></a> <a href="#" class="text-muted"><span class="fe fe-trash-2"></span></a> </div>
                        </li>
                        <li class="d-sm-flex">
                            <div> <i class="task-icon1"></i>
                                <h6 class="fw-semibold">Task Overdue<span class="text-muted fs-11 ms-2 fw-normal">14 June 2021</span></h6>
                                <p class="text-muted mb-0 fs-12">Petey Cruiser finished task <a href="#" class="fw-semibold"> Integrated management</a></p>
                            </div>
                            <div class="ms-auto d-md-flex me-3"> <a href="#" class="text-muted me-2"><span class="fe fe-edit"></span></a> <a href="#" class="text-muted"><span class="fe fe-trash-2"></span></a> </div>
                        </li>
                        <li class="d-sm-flex">
                            <div> <i class="task-icon1"></i>
                                <h6 class="fw-semibold">Task Overdue<span class="text-muted fs-11 ms-2 fw-normal">29 June 2021</span></h6>
                                <p class="text-muted mb-0 fs-12">Petey Cruiser finished task <a href="#" class="fw-semibold"> Integrated management</a></p>
                            </div>
                            <div class="ms-auto d-md-flex me-3"> <a href="#" class="text-muted me-2"><span class="fe fe-edit"></span></a> <a href="#" class="text-muted"><span class="fe fe-trash-2"></span></a> </div>
                        </li>
                        <li class="d-sm-flex">
                            <div> <i class="task-icon1"></i>
                                <h6 class="fw-semibold">Task Finished<span class="text-muted fs-11 ms-2 fw-normal">09 July 2021</span></h6>
                                <p class="text-muted fs-12">Adam Berry finished task on<a href="#" class="fw-semibold"> Project Management</a></p>
                            </div>
                            <div class="ms-auto d-md-flex me-3"> <a href="#" class="text-muted me-2"><span class="fe fe-edit"></span></a> <a href="#" class="text-muted"><span class="fe fe-trash-2"></span></a> </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps__rail-y" style="top: 0px; right: 0px;">
        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
    </div>
</div>
<!--/Sidebar-right-->
<!-- Country-selector modal-->
<div class="modal fade" id="country-selector">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content country-select-modal">
            <div class="modal-header">
                <h6 class="modal-title">Choose Country</h6><button aria-label="Close" class="btn-close" data-bs-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="row p-3">
                    <div class="col-lg-6">
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio1" checked="">
                        <label class="btn btn-country btn-lg btn-block" for="btnradio1"> <span class="country-selector"><img alt="" src="{{ url('assets/images/flags/us_flag.jpg') }}" class="me-3 language"></span>USA </label> <input type="radio" class="btn-check" name="btnradio" id="btnradio2">
                        <label class="btn btn-country btn-lg btn-block" for="btnradio2"> <span class="country-selector"><img alt="" src="../assets/images/flags/italy_flag.jpg" class="me-3 language"></span>Italy </label> <input type="radio" class="btn-check" name="btnradio" id="btnradio3">
                        <label class="btn btn-country btn-lg btn-block" for="btnradio3"> <span class="country-selector"><img alt="" src="../assets/images/flags/spain_flag.jpg" class="me-3 language"></span>Spain </label> <input type="radio" class="btn-check" name="btnradio" id="btnradio4">
                        <label class="btn btn-country btn-lg btn-block" for="btnradio4"> <span class="country-selector"><img alt="" src="../assets/images/flags/india_flag.jpg" class="me-3 language"></span>India </label> <input type="radio" class="btn-check" name="btnradio" id="btnradio5">
                        <label class="btn btn-country btn-lg btn-block" for="btnradio5"> <span class="country-selector"><img alt="" src="../assets/images/flags/french_flag.jpg" class="me-3 language"></span>French </label>
                    </div>
                    <div class="col-lg-6">
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio7">
                        <label class="btn btn-country btn-lg btn-block" for="btnradio7"> <span class="country-selector"><img alt="" src="{{ url('assets/images/flags/russia_flag.jpg') }}" class="me-3 language"></span>Russia </label> <input type="radio" class="btn-check" name="btnradio" id="btnradio8"> <label class="btn btn-country btn-lg btn-block" for="btnradio8"> <span class="country-selector"><img alt="" src="{{ url('assets/images/flags/germany_flag.jpg') }}" class="me-3 language"></span>Germany </label>
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio9">
                        <label class="btn btn-country btn-lg btn-block" for="btnradio9"> <span class="country-selector"><img alt="" src="{{ url('assets/images/flags/argentina.jpg') }}" class="me-3 language"></span>Argentina </label>
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio10">
                        <label class="btn btn-country btn-lg btn-block" for="btnradio10"> <span class="country-selector"><img alt="" src="{{ url('assets/images/flags/malaysia.jpg') }}" class="me-3 language"></span>Malaysia </label>
                        <input type="radio" class="btn-check" name="btnradio" id="btnradio11">
                        <label class="btn btn-country btn-lg btn-block" for="btnradio11"> <span class="country-selector"><img alt="" src="{{ url('assets/images/flags/turkey.jpg') }}" class="me-3 language"></span>Turkey </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Country-selector modal-->
<!-- FOOTER -->
<footer class="footer border-top-0 footer-1" style="margin-top: 50px;">
    <div class="container">
        <div class="row align-items-center text-center">
            <div class="col-lg-6 col-md-6 d-none d-md-block ">
                <div class="social">
                    <ul class="text-center m-0 ">
                        <li> <a class="social-icon" href=""><i class="fa fa-facebook"></i></a> </li>
                        <li> <a class="social-icon" href=""><i class="fa fa-twitter"></i></a> </li>
                        <li> <a class="social-icon" href=""><i class="fa fa-rss"></i></a> </li>
                        <li> <a class="social-icon" href=""><i class="fa fa-youtube"></i></a> </li>
                        <li> <a class="social-icon" href=""><i class="fa fa-linkedin"></i></a> </li>
                        <li> <a class="social-icon" href=""><i class="fa fa-google-plus"></i></a> </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 text-end privacy"> <a href="#" class="btn btn-link">Privacy</a> <a href="#" class="btn btn-link">Terms</a> <a href="#" class="btn btn-link">About Us</a> </div>
        </div>
        <div class="row align-items-center flex-row-reverse">
            <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center"> Copyright © 2022 <a href="#"></a>. All rights reserved. </div>
        </div>
    </div>
</footer><!-- FOOTER END -->
</div>
<!-- BACK-TO-TOP -->
<!-- <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a> -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>
<!-- JQUERY JS -->
<script src="{{ url('assets/js/jquery.min.js') }}"></script>
<!-- BOOTSTRAP JS -->
<script src="{{ url('assets/plugins/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ url('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- SPARKLINE JS-->
<script src="{{ url('assets/js/jquery.sparkline.min.js') }}"></script>
<!-- Sticky js -->
<script src="{{ url('assets/js/sticky.js') }}"></script>
<!-- SIDEBAR JS -->
<script src="{{ url('assets/plugins/sidebar/sidebar.js') }}"></script>
<!-- Perfect SCROLLBAR JS-->
<script src="{{ url('assets/plugins/p-scroll/perfect-scrollbar.js') }}"></script>
<script src="{{ url('assets/plugins/p-scroll/pscroll.js') }}"></script>
<script src="{{ url('assets/plugins/p-scroll/pscroll-1.js') }}"></script>

<!-- INTERNAL SELECT2 JS -->
<script src="{{ url('assets/plugins/select2/select2.full.min.js') }}"></script>
<!-- INTERNAL Data tables js-->
<script src="{{ url('assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/js/dataTables.bootstrap5.js') }}"></script>
<script src="{{ url('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>

<!-- INTERNAL Vector js -->
<script src="{{ url('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>

<script src="{{ url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SIDE-MENU JS-->
<script src="{{ url('assets/plugins/sidemenu/sidemenu1.js') }}"></script>
<!-- Sweet Alert js -->
<script src="{{ url('assets/plugins/sweet-alert/sweetalert.min.js') }}"></script>
<script src="{{ url('assets/js/sweet-alert.js') }}"></script>
<!-- INTERNAL INDEX JS -->
<script src="{{ url('assets/js/index1.js') }}"></script>
<script src="{{ url('assets/js/form-validation.js') }}"></script>
<!-- Color Theme js -->
<script src="{{ url('assets/js/themeColors.js') }}"></script>
<!-- CUSTOM JS -->

@yield('script')
<script src="{{ url('assets/js/custom.js') }}"></script>
</div>
<div class="jvectormap-tip"></div>
<script>
    setTimeout(function() {
        $('.alert.alert-danger').remove();
        $('.alert.alert-success').remove();
        $('.alert.alert-warning').remove();
    }, 5000);
</script>

<script>
    // $(window).on('load', function() {
    //     $('#global-loader').fadeOut('slow');
    // });
    $(window).on("load", function() {
        var urlHash = window.location.href.split("#")[1];
        $('html,body').animate({
            scrollTop: $('#' + urlHash).offset().top - 100
        }, 100);
    });

    setTimeout(function() {
        $('#global-loader').fadeOut('slow');
    }, 200);
</script>
</div>
<div class="sweet-overlay" tabindex="-1" style="opacity: -0.1; display: none;"></div>
<div class="sweet-alert hideSweetAlert" data-custom-class="" data-has-cancel-button="true" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: none; margin-top: -170px; opacity: -0.09;">
    <div class="sa-icon sa-error" style="display: none;">
        <span class="sa-x-mark">
            <span class="sa-line sa-left"></span>
            <span class="sa-line sa-right"></span>
        </span>
    </div>
    <div class="sa-icon sa-warning" style="display: block;">
        <span class="sa-body"></span>
        <span class="sa-dot"></span>
    </div>
    <div class="sa-icon sa-info" style="display: none;"></div>
    <div class="sa-icon sa-success" style="display: none;">
        <span class="sa-line sa-tip"></span>
        <span class="sa-line sa-long"></span>

        <div class="sa-placeholder"></div>
        <div class="sa-fix"></div>
    </div>
    <div class="sa-icon sa-custom" style="display: none;"></div>
    <h2>Alert</h2>
    <p style="display: block;">Waring alert</p>
    <fieldset>
        <input type="text" tabindex="3" placeholder="">
        <div class="sa-input-error"></div>
    </fieldset>
    <div class="sa-error-container">
        <div class="icon">!</div>
        <p>Not valid!</p>
    </div>
    <div class="sa-button-container">
        <button class="cancel" tabindex="2" style="display: inline-block; box-shadow: none;">Stay on the page</button>
        <div class="sa-confirm-button-container">
            <button class="confirm" tabindex="1" style="display: inline-block; background-color: rgb(140, 212, 245); box-shadow: rgba(140, 212, 245, 0.8) 0px 0px 2px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px inset;">Exit</button>
            <div class="la-ball-fall">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</div>

</body>

</html>