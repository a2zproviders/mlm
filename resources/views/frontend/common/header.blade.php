<html lang="en" dir="ltr" style="--primary01:rgba(108, 95, 252, 0.1); --primary02:rgba(108, 95, 252, 0.2); --primary03:rgba(108, 95, 252, 0.3); --primary06:rgba(108, 95, 252, 0.6); --primary09:rgba(108, 95, 252, 0.9);">

<head>
    <!-- META DATA -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="@yield('description')">
    <meta name="author" content="Pankaj Choudhary">
    <meta name="keywords" content="@yield('keywords')"> <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('assets/images/brand/favicon.ico') }}"> <!-- TITLE -->
    <meta name="_token" content="{{ csrf_token() }}">
    <title>MLM - @yield('title')</title> <!-- BOOTSTRAP CSS -->
    <link id="style" href="{{ url('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"> <!-- STYLE CSS -->
    <link href="{{ url('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('assets/css/dark-style.css') }}" rel="stylesheet">
    <link href="{{ url('assets/css/transparent-style.css') }}" rel="stylesheet">
    <link href="{{ url('assets/css/skin-modes.css') }}" rel="stylesheet">
    <!-- P-scroll bar css-->
    <link href="{{ url('assets/plugins/p-scroll/perfect-scrollbar.css') }}" rel="stylesheet">
    <!--- FONT-ICONS CSS -->
    <link href="{{ url('assets/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet">
    <!--- FONT-ICONS CSS -->
    <link href="{{ url('assets/css/icons.css') }}" rel="stylesheet"> <!-- INTERNAL Jvectormap css -->
    <link href="{{ url('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet"> <!-- SIDEBAR CSS -->
    <link href="{{ url('assets/plugins/sidebar/sidebar.css') }}" rel="stylesheet"> <!-- SELECT2 CSS -->
    <link href="{{ url('assets/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <!-- INTERNAL Data table css -->
    <link href="{{ url('assets/plugins/datatable/css/dataTables.bootstrap5.css') }}" rel="stylesheet">

    <link href="{{ url('assets/plugins/datatable/responsive.bootstrap5.css') }}" rel="stylesheet"> <!-- COLOR SKIN CSS -->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{ url('assets/colors/color1.css') }}"> <!-- INTERNAL Switcher css -->

    <meta http-equiv="imagetoolbar" content="no">
    <style type="text/css">
        /* input,textarea{-webkit-touch-callout:default;-webkit-user-select:auto;-khtml-user-select:auto;-moz-user-select:text;-ms-user-select:text;user-select:text} *{-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:-moz-none;-ms-user-select:none;user-select:none}  */
    </style>
    <style>
        .ps__rail-x {
            display: none !important;
        }

        .ps__rail-y {
            display: none !important;
        }
    </style>
    <style>
        footer {
            background: #fff !important;
            padding-top: 0.75rem;
            padding-bottom: 0.75rem;
            border-bottom: 1px solid #e9edf4;
        }

        .horizontal .fixed-header {
            position: fixed !important;
            z-index: 999;
        }

        .oneline-ellipsis {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .mobile-login {
            display: none !important;
        }

        .one-line-ellipsis {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
        }
        .two-line-ellipsis {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
        }
        .three-line-ellipsis {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
        }

        @media(max-width: 767px) {
            .responsive-navbar .dropdown {
                position: absolute;
                right: 5px;
                top: -63px;
            }

            .responsive-navbar .collapse.show .dropdown-menu.show {
                left: -378% !important;
                right: 15% !important;
            }

            .mobile-login {
                display: block !important;
            }
        }

        @media (min-width: 992px) {
            .product-height {
                height: 300px !important;
                object-fit: cover;
            }
        }
    </style>
    @yield('style')
</head>

<!-- <body class="app sidebar-mini ltr light-mode"> -->

<body class="app ltr horizontal horizontal-hover">
    <!-- app ltr horizontal horizontal-hover -->
    <div class="horizontalMenucontainer">
        <!-- GLOBAL-LOADER -->
        <div id="global-loader"> <img src="{{ url('assets/images/loader.svg') }}" class="loader-img" alt="Loader"> </div>
        <!-- /GLOBAL-LOADER -->
        <!-- PAGE -->
        <div class="page">
            <div class="page-main">
                <!-- app-Header -->
                <!-- <div class="app-header header sticky" style="margin-bottom: -74px;"> -->
                <div class="hor-header header sticky fixed-header">
                    <!-- <div class="container-fluid main-container"> -->
                    <div class="container main-container">
                        <div class="d-flex"> <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-bs-toggle="sidebar" href="#"></a> <!-- sidebar-toggle-->
                            <a class="logo-horizontal " href="{{ route('home') }}"> <img src="{{ url('assets/images/brand/logo.png') }}" class="header-brand-img desktop-logo" alt="logo">
                                <img src="{{ url('assets/images/brand/logo-3.png') }}" class="header-brand-img light-logo1" alt="logo">
                            </a> <!-- LOGO -->
                            <div class="d-flex order-lg-2 ms-auto header-right-icons">
                                <div class="dropdown d-lg-none d-md-block d-none"> <a href="#" class="nav-link icon" data-bs-toggle="dropdown"> <i class="fe fe-search"></i> </a>
                                    <div class="dropdown-menu header-search dropdown-menu-start">
                                        <div class="input-group w-100 p-2"> <input type="text" class="form-control" placeholder="Search....">
                                            <div class="input-group-text btn btn-primary"> <i class="fe fe-search" aria-hidden="true"></i> </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="navbar-toggler navresponsive-toggler d-md-none ms-auto collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="true" aria-label="Toggle navigation"> <span class="navbar-toggler-icon fe fe-more-vertical"></span> </button>
                                <div class="navbar navbar-collapse responsive-navbar p-0">
                                    <div class="collapse navbar-collapse show" id="navbarSupportedContent-4">
                                        <div class="d-flex order-lg-2">

                                            <div class="app-sidebar ps ps--active-y horizontal-main" style="padding-right: 50px; box-shadow: none; border-right: 0;">

                                                <div class="main-sidemenu is-expanded container" style="overflow: visible !important;">
                                                    <ul class="side-menu " style="overflow: visible !important;">

                                                        <li class="slide is-expanded">
                                                            <a class="side-menu__item active" data-bs-toggle="slide" href="{{ route('home') }}#home">
                                                                <!-- <i class="side-menu__icon fe fe-home"></i> -->
                                                                <span class="side-menu__label">Home</span>
                                                            </a>
                                                        </li>
                                                        <li> <a class="side-menu__item" href="{{ route('home') }}#about-us">
                                                                <!-- <i class="side-menu__icon fe fe-settings"></i> -->
                                                                <span class="side-menu__label">About Us</span>
                                                            </a> </li>
                                                        <li> <a class="side-menu__item" href="{{ route('kit') }}">
                                                                <!-- <i class="side-menu__icon fe fe-settings"></i> -->
                                                                <span class="side-menu__label">Product</span>
                                                            </a> </li>
                                                        <li> <a class="side-menu__item" href="{{ route('blog') }}">
                                                                <!-- <i class="side-menu__icon fe fe-settings"></i> -->
                                                                <span class="side-menu__label">Blog</span>
                                                            </a> </li>
                                                        <li> <a class="side-menu__item" href="{{ route('home') }}#faqs">
                                                                <!-- <i class="side-menu__icon fe fe-settings"></i> -->
                                                                <span class="side-menu__label">Faqs</span>
                                                            </a> </li>
                                                        <li> <a class="side-menu__item" href="{{ route('home') }}#contact-us">
                                                                <!-- <i class="side-menu__icon fe fe-settings"></i> -->
                                                                <span class="side-menu__label">Contact Us</span>
                                                            </a> </li>
                                                        @if(!Auth::guard('user')->user())
                                                        <li> <a class="side-menu__item" href="{{ route('auth.login') }}">
                                                                <!-- <i class="side-menu__icon fe fe-alert-circle"></i> -->
                                                                <span class="side-menu__label">Login</span>
                                                            </a> </li>
                                                        @endif
                                                        <!-- <li> <a class="side-menu__item" href="#" id="logout" data-url="{{ route('admin.logout') }}"><i class="side-menu__icon fe fe-alert-circle"></i><span class="side-menu__label">Logout</span></a> </li> -->

                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- <div class="dropdown d-flex header-settings"> <a href="javascript:void(0);" class="nav-link icon" data-bs-toggle="sidebar-right" data-target=".sidebar-right"> <i class="fe fe-align-right"></i> </a> </div> SIDE-MENU -->
                                            @if(Auth::guard('user')->user())
                                            <div class="dropdown d-flex profile-1"> <a href="#" data-bs-toggle="dropdown" class="nav-link leading-none d-flex">
                                                    <img src="{{ url('storage/user',Auth::guard('user')->user()->image) }}" alt="profile-user" class="avatar  profile-user brround cover-image"> </a>
                                                <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                                    <div class="drop-heading">
                                                        <div class="text-center">
                                                            <h5 class="text-dark mb-0 fs-14 fw-semibold"> {{ Auth::guard('user')->user()->name }} </h5> <small class="text-muted"> </small>
                                                        </div>
                                                    </div>
                                                    <div class="dropdown-divider m-0"></div>
                                                    <a class="dropdown-item" href="{{ route('user.dashboard') }}"> <i class="dropdown-icon fe fe-user"></i> User Dashboard </a>
                                                    <!-- <a class="dropdown-item" href="email-inbox.html"> <i class="dropdown-icon fe fe-mail"></i> Inbox <span class="badge bg-primary rounded-pill float-end">5</span> </a> -->
                                                    <!-- <a class="dropdown-item" href="lockscreen.html"> <i class="dropdown-icon fe fe-lock"></i> Lockscreen </a> -->
                                                    <!-- <a class="dropdown-item" href="{{ route('admin.setting') }}"> <i class="dropdown-icon fe fe-settings"></i> Settings </a> -->
                                                    <button class="dropdown-item" id="logout" data-url="{{ route('user.logout') }}"> <i class="dropdown-icon fe fe-alert-circle"></i> Logout </button>
                                                </div>
                                            </div>
                                            @else
                                            <div class="dropdown d-flex profile-1 mobile-login">
                                                <a href="{{ route('auth.login') }}" class="nav-link leading-none d-flex">
                                                    <i class="fa fa-sign-in avatar profile-user brround cover-image fs-20" style="padding-top: 2px; background: #fff; color: var(--primary-bg-color);"></i>
                                                    <!-- <img src="{{ url('assets/images/users/21.jpg') }}" alt="profile-user" class="avatar profile-user brround cover-image"> -->
                                                </a>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="jumps-prevent"></div> -->